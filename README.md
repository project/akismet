# Akismet

Used by millions of websites, [Akismet](https://akismet.com) filters out
hundreds of millions of spam comments from the Web every day. Add Akismet to
your site so you don't have to worry about spam again.

Although it is best known from the WordPress world, Akismet is a web service
and can be used from any system to check content for spamminess. With this
module, you can use it to filter comments on your Drupal site.

For a full description of the module, visit the
[project page](http://drupal.org/project/akismet).

For generic information about the Akismet service, please refer to the
[Akismet documentation](https://docs.akismet.com/).

Submit bug reports and feature suggestions, or track changes for the Drupal
module in the [issue queue](http://drupal.org/project/issues/akismet).


## Table of contents

- Requirements
- Installation
- Configuration
- Testing
- Troubleshooting
- Maintainers


## Requirements

* This module requires an active subscription for the Akismet web service. In
  case of personal, non-commercial sites, Akismet offers a free tier. In other
  situations, you will need to pay. See
  [Akismet pricing](https://akismet.com/plans) for details. Note that the
  maintainer is in no way affiliated with the commercial Akismet service.
* This module requires no modules outside of Drupal core.

## Recommended modules
When you get a lot of spam, your overview of unpublished comments can quickly
fill up. The
[Views Bulk Operations](https://www.drupal.org/project/views_bulk_operations)
module gives you the option to change the View in such a way that you can
select all filtered out comments at once, to delete them in one go.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

### Initial setup
1. Enable the module at *Administration » Extend*.
2. Go to *Administration » Configuration » Content authoring
   » Akismet content moderation*
3. Go to *Settings*
4. Fill in your Akismet API key and click *Save configuration*

If your site runs behind a reverse proxy or load balancer:

1. Open sites/default/settings.php in a text editor.
2. Ensure that the "reverse_proxy" settings are enabled and configured
   correctly.

Your site *must* send the actual/proper IP address for every site visitor to
Akismet. If this is not configured properly, your site's IP will be reported
for all sbumissions, including spam ones, which will have a detrimental effect
on the effectiveness of the spam filtering.

You can confirm that your configuration is correct by going to *Reports » Recent
log messages*.  In the details of each log entry, you should see a different IP
address for each site visitor in the *Hostname* field. If you see the same IP
address for different visitors, then your reverse proxy configuration is not
correct.

### For every form
The Akismet protection needs to be enabled and configured separately for each
form that you want to protect with Akismet:

1. Go to *Administration » Configuration » Content authoring
   » Akismet content moderation*
2. Add a form to protect and configure the options as desired.

### Permissions
The module exposes a *bypass* permission for every protected form.

## Testing
You may test Akismet without too much risk. In order to simulate a positive
(spam) result, set the comment author to "viagra-test-123" or comment author
email to "akismet-guaranteed-spam@example.com". For further details,
refer
[Akismet API documentation](https://akismet.com/development/api/#detailed-docs).

### Testing mode
The module contains a setting in the Advanced section to let Aksimet know you
are doing tests. You may set that to get more repeatable results while
testing.

## FAQ
Q: Why does Akismet not stop any spam on my form?

A: Are you sure you configured the correct form? You can configure the module to
log all interactions, not just warnings and errors, to make sure Akismet is
being triggered on your form.

Q: Can I protect other forms that are not listed?
Q: Can I protect a custom form?
Q: The form I want to protect is not offered as an option?

A: Out of the box, the Akismet module allows to protect Drupal core forms only.
However, the Akismet module provides an API for other modules.  Other modules
need to integrate with the Akismet module API to expose their forms.  The API
is extensively documented in akismet.api.php in this directory.

Q: Seriously, no [Webform](https://drupal.org/project/webform) support?

A: Sorry, no, not right now. You may want to contribute at
[issue 2973333](https://www.drupal.org/project/akismet/issues/2973333).

## Maintainers

### Current Maintainer

- Eelke Blok ([eelkeblok](http://drupal.org/u/eelkeblok))

### Past Maintainers
This includes maintainers for the original
[Mollom](https://en.wikipedia.org/wiki/Mollom) module that the D8-version of
this module was based on.

- Lisa Backer ([eshta](http://drupal.org/u/eshta))
- Nick Veenhof ([Nick_vh](https://www.drupal.org/u/Nick_vh))
- Jeff Eaton ([eaton](http://drupal.org/u/eaton))
- Katherine Senzee ([ksenzee](http://drupal.org/u/ksenzee))
- Tim Millwood ([timmillwood](http://drupal.org/u/timmillwood))
- Andrew Morton ([drewish](http://drupal.org/u/drewish))
- Daniel F. Kudwien ([sun](https://www.drupal.org/u/sun))
- Dries Buytaert ([Dries](http://drupal.org/u/dries))
