<?php

namespace Drupal\akismet\Utility;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Url;
use Drupal\akismet\Entity\FormInterface;

/**
 * Class AkismetUtilities.
 *
 * Various static utility methods.
 *
 * @package Drupal\akismet\Utility
 */
class AkismetUtilities {

  /**
   * Recursive helper function to flatten nested form values.
   *
   * Takes a potentially nested array and returns all non-empty string values in
   * nested keys as new indexed array.
   */
  public static function flattenFormValue($values): array {
    $flat_values = [];
    foreach ($values as $value) {
      if (is_array($value)) {
        // Only text fields are supported at this point; their values are in the
        // 'summary' (optional) and 'value' keys.
        if (isset($value['value'])) {
          if (isset($value['summary']) && $value['summary'] !== '') {
            $flat_values[] = $value['summary'];
          }
          if ($value['value'] !== '') {
            $flat_values[] = $value['value'];
          }
        }
        elseif (!empty($value)) {
          $flat_values = array_merge($flat_values, self::flattenFormValue($value));
        }
      }
      elseif (is_string($value) && strlen($value)) {
        $flat_values[] = $value;
      }
    }
    return $flat_values;
  }

  /**
   * Returns the (last known) status of the configured Akismet API keys.
   *
   * @param bool $force
   *   (optional) Boolean whether to ignore the cached state and re-check.
   *   Defaults to FALSE.
   * @param bool $update
   *   (optional) Whether to update Akismet with locally stored configuration.
   *   Defaults to FALSE.
   *
   * @return array
   *   An associative array describing the current status of the module:
   *   - isConfigured: Boolean whether Akismet API keys have been configured.
   *   - isVerified: Boolean whether Akismet API keys have been verified.
   *   - response: The response error code of the API verification request.
   *   - ...: The full site resource, as returned by the Akismet API.
   *
   * @see akismet_requirements()
   */
  public static function getApiKeyStatus($force = FALSE, $update = FALSE): array {
    $status = drupal_static(__FUNCTION__);

    if (is_array($status) && !$force) {
      return $status;
    }

    $testing_mode = (int) \Drupal::config('akismet.settings')
      ->get('test_mode.enabled');

    // Re-check configuration status.
    /** @var \Drupal\akismet\Client\DrupalClient $akismet */
    $akismet = \Drupal::service('akismet.client');
    $status = [
      'isConfigured' => FALSE,
      'isVerified' => FALSE,
      'isTesting' => (bool) $testing_mode,
      'response' => NULL,
      'key' => $akismet->loadConfiguration('key'),
    ];
    $status['isConfigured'] = !empty($status['key']);

    if ($testing_mode || $status['isConfigured']) {
      $response = $akismet->verifyKey($status['key']);
      $status['response'] = $response;

      if ($response === TRUE) {
        $status['isVerified'] = TRUE;
        Logger::addMessage([
          'message' => 'API key is valid.',
        ], RfcLogLevel::INFO);
      }
      elseif ($response === FALSE) {
        Logger::addMessage([
          'message' => 'Invalid API key.',
        ], RfcLogLevel::ERROR);
      }
      else {
        // A NETWORK_ERROR and other possible responses may be caused by the
        // client-side environment, but also by Akismet service downtimes. Try
        // to recover as soon as possible.
        Logger::addMessage([
          'message' => 'API keys could not be verified.',
        ], RfcLogLevel::ERROR);
      }
    }

    return $status;
  }

  /**
   * Get API key status and display a warning message.
   *
   * Gets the status of Akismet's API key configuration and also displays a
   * warning message if the Akismet API keys are not configured. To be used
   * within the Akismet administration pages only.
   *
   * @param bool $force
   *   (optional) Boolean whether to ignore the cached state and re-check.
   *   Defaults to FALSE.
   * @param bool $update
   *   (optional) Whether to update Akismet with locally stored configuration.
   *   Defaults to FALSE.
   *
   * @return array
   *   An associative array describing the current status of the module:
   *   - isConfigured: Boolean whether Akismet API keys have been configured.
   *   - isVerified: Boolean whether Akismet API keys have been verified.
   *   - response: The response error code of the API verification request.
   *   - ...: The full site resource, as returned by the Akismet API.
   *
   * @see AkismetUtilities::getAPIKeyStatus()
   */
  public static function getAdminApiKeyStatus($force = FALSE, $update = FALSE): array {
    $status = AkismetUtilities::getApiKeyStatus($force, $update);
    if (empty($_POST) && !$status['isVerified']) {
      // Fetch and display requirements error message, without re-checking.
      \Drupal::moduleHandler()->loadInclude('akismet', 'install');
      $requirements = akismet_requirements('runtime', FALSE);
      if (isset($requirements['akismet']['description'])) {
        \Drupal::messenger()->addMessage($requirements['akismet']['description'], 'error');
      }
    }
    return $status;
  }

  /**
   * Outputs a warning message about enabled testing mode (once).
   */
  public static function displayAkismetTestModeWarning() {
    // Messenger::addMessage() starts a session and disables page caching, which
    // breaks cache-related tests. Thus, tests set the verbose variable to TRUE.
    if (\Drupal::state()->get('akismet.omit_warning') ?: FALSE) {
      return;
    }

    if (\Drupal::config('akismet.settings')->get('test_mode.enabled') && empty($_POST)) {
      $admin_message = '';
      $hasPermission = \Drupal::currentUser()->hasPermission('administer akismet');
      $isNotAkismetSettings = \Drupal::routeMatch()->getRouteName() != 'akismet.settings';
      if ($hasPermission && $isNotAkismetSettings) {
        $admin_message = t(
          'Visit the <a href="@settings-url">Akismet settings page</a> to disable it.',
          ['@settings-url' => Url::fromRoute('akismet.settings')->toString()]
        );
      }
      $message = t('Akismet testing mode is still enabled. @admin-message',
        ['@admin-message' => $admin_message]
      );
      \Drupal::messenger()->addMessage($message, 'warning', FALSE);
    }
  }

  /**
   * Handle the fallback settings.
   *
   * When configuration is set to block submissions when Akismet servers are
   * unavailable, set an error on the passed form (when available) and output
   * an error message..
   *
   * @param \Drupal\Core\Form\FormStateInterface|null $form_state
   *   Form state for the currently handled form.
   * @param string $element_name
   *   Element name to set an error on when appropriate.
   */
  public static function handleFallback(FormStateInterface $form_state = NULL, $element_name = 'form_id') {
    $fallback = \Drupal::config('akismet.settings')->get('fallback');
    if ($fallback == FormInterface::AKISMET_FALLBACK_BLOCK) {
      $block_message = t("The spam filter installed on this site is currently unavailable. Per site policy, we are unable to accept new submissions until that problem is resolved. Please try resubmitting the form in a couple of minutes.");
      if (!empty($form_state) && $form_state->isProcessingInput() && !empty($element_name)) {
        $form_state->setErrorByName($element_name, $block_message);
      }
      else {
        \Drupal::messenger()->addMessage($block_message, 'error');
      }
    }
  }

}
