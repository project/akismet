<?php

namespace Drupal\akismet\EventSubscriber;

use Drupal\akismet\Utility\AkismetUtilities;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\akismet\Utility\Logger;

/**
 * Class Subscriber.
 *
 * Event subscriber implementing different event subscriptions.
 *
 * @package Drupal\akismet\EventSubscriber
 */
class Subscriber implements EventSubscriberInterface {

  /**
   * The module handler.
   *
   * @var ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @param ModuleHandlerInterface $moduleHandler
   */
  public function __construct(ModuleHandlerInterface $moduleHandler) {
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Set a low value to start as early as possible.
    $events[KernelEvents::REQUEST][] = ['onRequest', -100];

    // Why only large positive value works here?
    $events[KernelEvents::TERMINATE][] = ['onTerminate', 1000];

    return $events;
  }

  /**
   * Acts on KernelEvents::REQUEST.
   */
  public function onRequest() {
    // On all Akismet administration pages, check the module configuration and
    // display the corresponding requirements error, if invalid.
    $url = Url::fromRoute('<current>');
    $current_path = $url->toString();
    if (empty($_POST) && strpos($current_path, 'admin/config/content/akismet') === 0 && \Drupal::currentUser()->hasPermission('administer akismet')) {
      // Re-check the status on the settings form only.
      $status = AkismetUtilities::getApiKeyStatus($current_path == 'admin/config/content/akismet/settings');
      if ($status !== TRUE) {
        // Fetch and display requirements error message, without re-checking.
        $this->moduleHandler->loadInclude('akismet', 'install');
        $requirements = akismet_requirements('runtime', FALSE);
        if (isset($requirements['akismet']['description'])) {
          \Drupal::messenger()->addMessage($requirements['akismet']['description'], 'error');
        }
      }
    }
  }

  /**
   * Acts on KernelEvents::TERMINATE.
   */
  public function onTerminate() {
    Logger::writeLog();
  }

}
