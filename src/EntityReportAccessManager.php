<?php

namespace Drupal\akismet;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\akismet\Controller\FormController;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Class EntityReportAccessManager.
 *
 * Determine whether a user has access to report a particular entity.
 *
 * @package Drupal\akismet
 */
class EntityReportAccessManager {

  /**
   * Determines if the user specified has access to report the entity.
   *
   * @param \Drupal\core\Entity\EntityInterface $entity
   *   The entity to check access for.
   * @param string $form_id
   *   The form that is protected for this entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account to use.  If null, use the current user.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Indicates whether the user should have access to the report for the
   *   given entity and form.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function accessReport(
    EntityInterface $entity,
    string $form_id,
    AccountInterface $account = NULL): AccessResultInterface {
    // Check if the user has access to this comment.
    $result = $entity->access('edit', $account, TRUE)
      ->andIf($entity->access('update', $account, TRUE));
    if (!$result->isAllowed()) {
      return $result;
    }
    // Check if this entity type is protected.
    $form_entity = \Drupal::entityTypeManager()->getStorage('akismet_form')->load($form_id);
    if (empty($form_entity)) {
      return new AccessResultForbidden();
    }
    // Check any specific report access callbacks.
    $forms = FormController::getProtectableForms();
    $info = $forms[$form_id];
    if (empty($info)) {
      // Orphan form protection.
      return new AccessResultForbidden();
    }

    $report_access_callbacks = [];
    $access_permissions = [];

    // If there is a 'report access callback' add it to the list.
    if (isset($info['report access callback'])
      && function_exists($info['report access callback'])
      && !in_array($info['report access callback'], $report_access_callbacks)) {
      $report_access_callbacks[] = $info['report access callback'];
    }
    // Otherwise add any access permissions.
    elseif (isset($info['report access']) && !in_array($info['report access'], $access_permissions)) {
      $access_permissions += $info['report access'];
    }

    foreach ($report_access_callbacks as $callback) {
      if (!$callback($entity->getEntityTypeId(), $entity->id())) {
        return new AccessResultForbidden();
      }
    }

    foreach ($access_permissions as $permission) {
      if (empty($account)) {
        $account = \Drupal::currentUser();
      }
      if (!$account->hasPermission($permission)) {
        return new AccessResultForbidden();
      }
    }
    return new AccessResultAllowed();
  }

}
