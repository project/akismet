<?php

namespace Drupal\akismet\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\akismet\Client\DrupalClient;
use Drupal\akismet\Utility\AkismetUtilities;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a form that configures devel settings.
 */
class Settings extends ConfigFormBase {
  /**
   * Block all submissions of protected forms when server is unreachable.
   */
  const AKISMET_FALLBACK_BLOCK = 0;

  /**
   * Accept all submissions of protected forms when server is unreachable.
   */
  const AKISMET_FALLBACK_ACCEPT = 1;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'akismet_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'akismet.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    AkismetUtilities::displayAkismetTestModeWarning();

    $config = $this->config('akismet.settings');

    $status = AkismetUtilities::getAdminApiKeyStatus();
    if ($status['isVerified'] && !$config->get('test_mode.enabled')) {
      $this->messenger()->addMessage(t('Akismet verified your key. The service is operating correctly.'));
    }

    // Keys are not #required to allow to install this module and configure it
    // later.
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t(
        'To obtain an API key, <a href="@signup-url">sign up</a> or log in to your <a href="@account-url">account</a>, add a subscription for this site, and copy the key into the field below.',
        [
          '@signup-url' => 'https://akismet.com/signup',
          '@account-url' => 'https://akismet.com/account',
        ]
      ),
    ];

    $form['fallback'] = [
      '#type' => 'radios',
      '#title' => t('When the Akismet service is unavailable'),
      '#default_value' => $config->get('fallback'),
      '#options' => [
        Settings::AKISMET_FALLBACK_ACCEPT => t('Accept all form submissions'),
        Settings::AKISMET_FALLBACK_BLOCK => t('Block all form submissions'),
      ],
    ];

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced configuration'),
      '#open' => FALSE,
    ];
    // Lower severity numbers indicate a high severity level.
    $form['advanced']['log_level'] = [
      '#type' => 'radios',
      '#title' => t('Akismet logging level warning'),
      '#options' => [
        RfcLogLevel::WARNING => $this->t('Only log warnings and errors'),
        RfcLogLevel::DEBUG => $this->t('Log all Akismet messages'),
      ],
      '#default_value' => $config->get('log_level'),
    ];
    $timeout = $config->get('connection_timeout_seconds');
    $form['advanced']['connection_timeout_seconds'] = [
      '#type' => 'number',
      '#title' => $this->t('Time-out when attempting to contact Akismet servers.'),
      '#description' => $this->t('This is the length of time that a call to Akismet will wait before timing out.'),
      '#default_value' => !empty($timeout) ? $config->get('connection_timeout_seconds') : 3,
      '#size' => 5,
      '#field_suffix' => $this->t('seconds'),
      '#required' => TRUE,
    ];
    $form['advanced']['testing_mode'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable testing mode'),
      '#return_value' => TRUE,
      '#default_value' => $config->get('test_mode.enabled'),
      '#description' => $this->t('Let Akismet know you are testing. This is mostly useful for somewhat repeatable test results for automated testing. For details and triggering guaranteed spam responses (regardless of using test mode), refer to the <a href="https://akismet.com/development/api/#detailed-docs">Akismet API documentation</a>.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('akismet.settings');

    $config->set('api_key', $values['api_key'])
      ->set('fallback', $values['fallback'])
      ->set('test_mode.enabled', $values['testing_mode'])
      ->set('log_level', $values['log_level'])
      ->set('connection_timeout_seconds', $values['connection_timeout_seconds'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
