<?php

namespace Drupal\akismet\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the interface for index entities.
 */
interface FormInterface extends ConfigEntityInterface {

  /**
   * Form protection mode: No protection.
   */
  const AKISMET_MODE_DISABLED = 0;

  /**
   * Form protection mode: Text analysis.
   */
  const AKISMET_MODE_ANALYSIS = 2;

  /**
   * Block all submissions of protected forms when server communication fails.
   */
  const AKISMET_FALLBACK_BLOCK = 0;

  /**
   * Accept all submissions of protected forms when server communication fails.
   */
  const AKISMET_FALLBACK_ACCEPT = 1;

  /**
   * Initialize object based on configuration and form definitions.
   *
   * Set defaults based on a form definition if the entity is new, otherwise
   * gets the form information for the existing form configuration.
   *
   * This somewhat corresponds to akismet_form_new in previous versions.
   *
   * @param string $form_id
   *   The id of the form that will be protected.
   *
   * @return array
   *   An array of default protected form information.
   */
  public function initialize($form_id = NULL): array;

  /**
   * Get the fields to be checked within the form.
   *
   * @return array
   *   An array of fields to be checked within the form.
   */
  public function getEnabledFields(): array;

  /**
   * Sets the array of fields that should be checked within the form as body.
   *
   * @param array $fields
   *   The array of fields that should be checked within the form as body.
   *
   * @return \Drupal\akismet\Entity\FormInterface
   *   The changed object.
   */
  public function setEnabledFields(array $fields): FormInterface;

  /**
   * Get the unsure action for this form.
   *
   * What to do when Akismet returns an unsure for content submitted through
   * this form.
   *
   * @return string
   *   Either 'moderate' or 'discard'.
   */
  public function getUnsure(): string;

  /**
   * Set the unsure action for this form.
   *
   * Sets how the module will handle content submitted through this form that
   * Akismet is unsure about.
   *
   * @param string $handling
   *   One of 'moderate' or 'discard'.
   *
   * @return \Drupal\akismet\Entity\FormInterface
   *   The changed object.
   */
  public function setUnsure(string $handling): FormInterface;

  /**
   * Get the discard action for this form.
   *
   * Indicates if the module should discard spam from this form or keep for
   * moderation.
   *
   * @return bool
   *   True when post should be discarded, false when post should be retained.
   */
  public function getDiscard(): bool;

  /**
   * Set the discard action for this form.
   *
   * Sets whether the module should discard spam (TRUE) or keep for moderation
   * (FALSE).
   *
   * @param bool $discard
   *   True when post should be discarded, false when post should be retained.
   *
   * @return \Drupal\akismet\Entity\FormInterface
   *   The changed object.
   */
  public function setDiscard(bool $discard): FormInterface;

  /**
   * Gets the mapping of field values for Akismet submissions.
   *
   * @return array
   *   The mapping of field values.
   */
  public function getMapping(): array;

  /**
   * Sets the mapping of field values for Akismet submissions.
   *
   * @param array $mapping
   *   The mapping of field values.
   *
   * @return \Drupal\akismet\Entity\FormInterface
   *   The changed object.
   */
  public function setMapping(array $mapping): FormInterface;

  /**
   * Gets the name of the module that owns the form being protected.
   *
   * @return string
   *   The module's name.
   */
  public function getModule(): string;

  /**
   * Sets the name of the module that owns the forms being protected.
   *
   * @param string $module
   *   The module's name.
   *
   * @return \Drupal\akismet\Entity\FormInterface
   *   The changed object.
   */
  public function setModule(string $module): FormInterface;

  /**
   * Get the entity id of the entity form being protected.
   *
   * @return string
   *   The entity id of the entity form being protected.
   */
  public function getEntity(): string;

  /**
   * Set the entity id of the entity form being protected.
   *
   * @param string $entity
   *   The entity id of the entity form that should be protected.
   *
   * @return \Drupal\akismet\Entity\FormInterface
   *   The changed object.
   */
  public function setEntity(string $entity): FormInterface;

  /**
   * Get the bundle id of the entity bundle being protected.
   *
   * @return string
   *   The bundle id of the entity bundle being protected.
   */
  public function getBundle(): string;

  /**
   * Sets the entity bundle id of the entity bundle being protected.
   *
   * @param string $bundle
   *   The bundle id of the entity bundle that should be protected.
   *
   * @return \Drupal\akismet\Entity\FormInterface
   *   The changed object,
   */
  public function setBundle(string $bundle): FormInterface;

}
