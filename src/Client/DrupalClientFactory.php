<?php

namespace Drupal\akismet\Client;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\ClientInterface;

/**
 * Class DrupalClientFactory.
 *
 * A service factory to determine the correct version of the Drupal client
 * service.
 *
 * This will return the DrupalClient or a test service depending on if testing
 * mode is enabled.
 *
 * @package Drupal\akismet\Client
 * @package Drupal\akismet\Client
 */
class DrupalClientFactory {

  /**
   * Factory method to select the correct Akismet client service.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory in order to retrieve Akismet settings data.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   An http client.
   *
   * @return DrupalClientInterface
   *   A DrupalClient based on the various testing settings.
   */
  public static function createDrupalClient(ConfigFactoryInterface $config_factory, ClientInterface $http_client) {
    $akismet_settings = $config_factory->get('akismet.settings');
    $state = \Drupal::state();
    if ($state->get('akismet.testing_use_local_invalid') ?: FALSE) {
      $class = DrupalTestInvalid::class;
    }
    elseif ($state->get('akismet.testing_use_local') ?: FALSE) {
      $class = DrupalTestLocalClient::class;
    }
    elseif ($akismet_settings->get('test_mode.enabled')) {
      $class = DrupalTestClient::class;
    }
    else {
      $class = DrupalClient::class;
    }

    return $class::create(\Drupal::getContainer());
  }

}
