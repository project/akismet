<?php

namespace Drupal\akismet\Client;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\akismet\Utility\Logger;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Stream;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DrupalClient.
 *
 * Drupal-specific implementation of the Akismet PHP client.
 *
 * @package Drupal\akismet\Client
 */
class DrupalClient extends Client implements DrupalClientInterface, ContainerInjectionInterface {

  /**
   * The settings configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  public $config;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  public $client;

  /**
   * Mapping of configuration names to Drupal variables.
   *
   * @var array
   *
   * @see Akismet::loadConfiguration()
   */
  protected $configurationMap = [
    'key' => 'api_key',
  ];

  /**
   * The install profile for the site.
   *
   * @var string
   */
  protected $installProfile;

  /**
   * DrupalClient constructor.
   *
   * Overrides the connection timeout based on module configuration.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   * @param string $profile_name
   *   The name of the profile. Inject container variable.
   *
   * @see \Drupal\akismet\Client\Client::__construct()
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, string $profile_name) {
    $this->config = $config_factory->getEditable('akismet.settings');
    $this->requestTimeout = $this->config->get('connection_timeout_seconds');
    $this->client = $http_client;
    $this->installProfile = $profile_name;
    parent::__construct();
    $this->requestTimeout = $this->config->get('connection_timeout_seconds');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('http_client'),
      $container->getParameter('install_profile')
    );
  }

  /**
   * Implements Akismet::loadConfiguration().
   */
  public function loadConfiguration($name) {
    $name = $this->configurationMap[$name];
    return $this->config->get($name);
  }

  /**
   * Implements Akismet::saveConfiguration().
   */
  public function saveConfiguration($name, $value) {
    // Save it to the class properties if applicable.
    if (property_exists('\Drupal\akismet\Client\DrupalClient', $name)) {
      $this->{$name} = $value;
    }
    // Persist in Drupal too.
    $name = $this->configurationMap[$name];
    $this->config->set($name, $value)->save();
  }

  /**
   * Implements Akismet::deleteConfiguration().
   */
  public function deleteConfiguration($name) {
    $name = $this->configurationMap[$name];
    $this->config->clear($name)->save();
  }

  /**
   * Implements Akismet::getClientInformation().
   */
  public function getClientInformation() {
    // Retrieve Drupal distribution and installation profile information.
    $profile_info = \Drupal::service('extension.list.profile')
      ->getExtensionInfo($this->installProfile) +
      [
        'distribution_name' => 'Drupal',
        'version' => \Drupal::VERSION,
      ];

    // Retrieve Akismet module information.
    $akismet_info = \Drupal::service('extension.list.module')->getExtensionInfo('akismet');
    if (empty($akismet_info['version'])) {
      // Manually build a module version string for repository checkouts.
      $akismet_info['version'] = '8.x-1.x-dev';
    }

    $data = [
      'platformName' => $profile_info['distribution_name'],
      'platformVersion' => $profile_info['version'],
      'clientName' => $akismet_info['name'],
      'clientVersion' => $akismet_info['version'],
    ];
    return $data;
  }

  /**
   * Overrides Akismet::getSiteURL().
   */
  public function getSiteUrl() {
    return $GLOBALS['base_url'];
  }

  /**
   * {@inheritdoc}
   */
  public function writeLog() {
    foreach ($this->log as $entry) {
      $response = $entry['response'];
      $response = ($response instanceof Stream) ? $response->getContents() : $response;

      $entry['Request: ' . $entry['request']] = !empty($entry['data']) ? $entry['data'] : NULL;
      unset($entry['request'], $entry['data']);

      $entry['Request headers:'] = $entry['headers'];
      unset($entry['headers']);

      $entry['Response: ' . $entry['response_code'] . ' ' . $entry['response_message'] . ' (' . number_format($entry['response_time'], 3) . 's)'] = $response;
      unset($entry['response'], $entry['response_code'], $entry['response_message'], $entry['response_time']);

      // The client class contains the logic for recovering from certain errors,
      // and log messages are only written after that happened. Therefore, we
      // can normalize the severity of all log entries to the overall success or
      // failure of the attempted request.
      // @see Akismet::query()
      Logger::addMessage($entry, $this->lastResponse->isError ? RfcLogLevel::WARNING : NULL);
    }

    // After writing log messages, empty the log.
    $this->purgeLog();
  }

  /**
   * {@inheritdoc}
   */
  protected function request(string $method, string $server, string $path, string $query = NULL, array $headers = []) {
    $options = [
      'timeout' => $this->requestTimeout,
    ];
    if (isset($query)) {
      if ($method === 'GET') {
        $path .= '?' . $query;
      }
      else {
        $options['body'] = $query;
      }
    }
    $request = new Request($method, $server . '/' . $path, $headers);

    try {
      $response = $this->client->send($request, $options);
    }
    catch (ClientException $e) {
      $akismet_response = [
        'code' => $e->getCode(),
        'message' => $e->getResponse()->getReasonPhrase(),
        'headers' => $e->getResponse()->getHeaders(),
        'body' => $e->getResponse()->getBody()->getContents(),
      ];
    }
    catch (\Exception $e) {
      Logger::addMessage(
        [
          'message' => 'failed to connect. Message @message',
          'arguments' => ['@message' => $e->getMessage()],
        ], RfcLogLevel::ERROR);
      return (object) [
        'code' => '0',
        'message' => $e->getMessage(),
        'headers' => [],
        'body' => '',
      ];
    }

    if (empty($akismet_response)) {
      $akismet_response = [
        'code' => $response->getStatusCode(),
        'message' => ($response->getStatusCode() >= 200 && $response->getStatusCode() < 300) ? $response->getReasonPhrase() : NULL,
        'headers' => $response->getHeaders(),
        'body' => (string) $response->getBody(),
      ];
    }
    // Convert headers to expected and consistent format.
    $headers = [];
    foreach ($akismet_response['headers'] as $key => $header) {
      $headers[mb_strtolower($key)] = $header[0];
    }
    $akismet_response['headers'] = $headers;
    return (object) $akismet_response;
  }

}
