<?php

namespace Drupal\akismet\Client;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DrupalTestInvalid.
 *
 * Drupal Akismet client implementation of an invalid server.
 *
 * @package Drupal\akismet\Client
 */
class DrupalTestInvalid extends DrupalTestLocalClient {

  /**
   * Number of attempts done.
   *
   * @var int
   */
  private $currentAttempt = 0;

  /**
   * Variable to be store the original server when we replace with an invalud.
   *
   * @var string
   */
  private $originalServer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->originalServer = $instance->server;
    $instance->configurationMap['server'] = 'test_mode.invalid.api_endpoint';
    $instance->saveConfiguration('server', 'fake-host');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function query(string $method, string $path, array $data, bool $authenticate = TRUE) {
    $this->currentAttempt = 0;
    return parent::query($method, $path, $data, $authenticate);
  }

  /**
   * {@inheritdoc}
   */
  protected function handleRequest(string $method, string $server, string $path, array $data) {
    // Akismet::$server is replaced with an invalid server, so all requests will
    // result in a network error. However, if the
    // 'akismet_testing_server_failover' variable is set to TRUE, then the last
    // request attempt will succeed.
    $this->currentAttempt++;

    if (\Drupal::state()->get('akismet_testing_server_failover', FALSE) && $this->currentAttempt == $this->requestMaxAttempts) {
      $server = strtr($server, [$this->server => $this->originalServer]);
    }
    return parent::handleRequest($method, $server, $path, $data);
  }

}
