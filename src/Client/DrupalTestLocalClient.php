<?php

namespace Drupal\akismet\Client;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DrupalTestLocalClient.
 *
 * Drupal Akismet client implementation using local dummy/fake REST server.
 *
 * @package Drupal\akismet\Client
 */
class DrupalTestLocalClient extends DrupalTestClient {

  /**
   * The test server location to use overriding any configuration.
   *
   * @var string
   */
  public $server;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    // Replace server/endpoint with our local fake server.
    $server = \Drupal::request()->getHttpHost() . '/akismet-test';
    $instance->server = $server;

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function loadConfiguration($name) {
    if ($name === 'server') {
      return $this->server;
    }
    return parent::loadConfiguration($name);
  }

  /**
   * {@inheritdoc}
   */
  public function saveConfiguration($name, $value) {
    // Save it to the class properties if applicable.
    if ($name === 'server') {
      $this->server = $value;
    }
    else {
      parent::saveConfiguration($name, $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getAkismetUrl($authenticate) {
    return 'http://' . $this->server;
  }

  /**
   * {@inheritdoc}
   */
  protected function request(string $method, string $server, string $path, string $query = NULL, array $headers = []) {
    // Passes-through SimpleTest assertion HTTP headers from child-child-site
    // and triggers errors to make them appear in parent site (where tests are
    // run).
    //
    // @todo Remove when in core.
    // @see http://drupal.org/node/875342
    $response = parent::request($method, $server, $path, $query, $headers);
    $keys = preg_grep('@^x-drupal-assertion-@', array_keys($response->headers));
    foreach ($keys as $key) {
      $header = $response->headers[$key];
      $header = unserialize(urldecode($header));
      $message = strtr('%type: @message in %function (line %line of %file).', [
        '%type' => $header[1],
        '@message' => $header[0],
        '%function' => $header[2]['function'],
        '%line' => $header[2]['line'],
        '%file' => $header[2]['file'],
      ]);
      if ($header[1] != 'User deprecated function') {
        trigger_error($message, E_USER_ERROR);
      }
    }

    return $response;
  }

}
