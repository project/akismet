<?php

namespace Drupal\akismet\Client;

/**
 * Represents a response from the Akismet API.
 *
 * @property-read int $code
 *   The response code.
 * @property-read array $message
 *   The response message.
 * @property-read array $headers
 *   Associative array of response headers, keyed by header name.
 * @property-read string $body
 *   The body of the response. Usually one word: 'true', 'false', 'valid'.
 * @property-read bool $isError
 *   A flag indicating whether the response indicated an error.
 */
class AkismetResponse {

  /**
   * The response code.
   *
   * @var int
   */
  protected $code;

  /**
   * The response message.
   *
   * @var string
   */
  protected $message;

  /**
   * Associative array of response headers, keyed by header name.
   *
   * @var array
   */
  protected $headers;

  /**
   * The body of the response. Usually one word: 'true', 'false', 'valid'.
   *
   * @var string
   */
  protected $body;

  /**
   * A flag indicating whether the response indicated an error.
   *
   * @var bool
   */
  protected $isError;

  /**
   * AkismetResponse constructor.
   *
   * @param object $data
   *   A data object such as a Guzzle response with at least member variables
   *   headers. body and code. `headers` is an array with keys representing HTTP
   *   header names. Values are the value of the respective header.
   */
  public function __construct($data) {
    $this->headers = $data->headers;
    $this->body = $data->body;
    $this->code = $data->code;

    // Determine basic error condition based on HTTP status code.
    $this->isError = ($this->code < 200 || $this->code >= 300);

    // The Akismet API returns 200 OK even when there's an error, so it's
    // hard to be sure what kind of response this is. One way we can be sure the
    // request was malformed is if we receive the header 'X-Akismet-Debug-Help'.
    if (!empty($this->headers['x-akismet-debug-help'])) {
      $this->isError = TRUE;
      $this->code = Client::REQUEST_ERROR;
      $this->message = $data->headers['x-akismet-debug-help'];
    }

    /*
     * @todo Handle error codes.
     *   https://www.drupal.org/project/akismet/issues/3216264
     */

    // Another clue is if we receive the body text "Invalid API key.".
    if ($this->body === 'Invalid API key.') {
      $this->isError = TRUE;
      $this->code = Client::AUTH_ERROR;
      $this->message = $this->body;
    }
  }

  /**
   * Magic getter.
   *
   * @param string $name
   *   The name of the member variable to retrieve.
   *
   * @return mixed
   *   The value of the member variable.
   */
  public function __get(string $name) {
    return $this->{$name};
  }

  /**
   * Retrieve the GUID from the response headers.
   *
   * @return false|mixed
   *   The contents of the header or FALSE when not available.
   */
  public function guid() {
    if (!empty($this->headers['x-akismet-guid'])) {
      return $this->headers['x-akismet-guid'];
    }
    return FALSE;
  }

}
