<?php

namespace Drupal\akismet\Client;

/**
 * Drupal Akismet client implementation using Akismet testing mode.
 */
class DrupalTestClient extends DrupalClient {

  /**
   * {@inheritdoc}
   */
  public function query(string $method, string $path, array $data, bool $authenticate = TRUE) {
    $data['is_test'] = 1;
    return parent::query($method, $path, $data, $authenticate);
  }

}
