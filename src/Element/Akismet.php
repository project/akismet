<?php

namespace Drupal\akismet\Element;

use Drupal\akismet\Utility\AkismetUtilities;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\akismet\Entity\FormInterface;

/**
 * Provides a form element for storage of internal information.
 *
 * @FormElement("akismet")
 */
class Akismet extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => FALSE,
      '#process' => [
        [$class, 'processAkismet'],
      ],
      '#tree' => TRUE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    return $input;
  }

  /**
   * Process callback for #type 'akismet'.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The form containing the element.
   *
   * @return array
   *   The processed element.
   *
   * @see akismet_form_alter()
   * @see akismet_element_info()
   */
  public static function processAkismet(array $element, FormStateInterface $form_state, array $form): array {
    $akismet = $form_state->getValue('akismet');
    $akismet = $akismet ? $akismet : [];
    // Allow overriding via hook_form_alter to set akismet override properties.
    if (isset($form['#akismet']) && is_array($form['#akismet'])) {
      $akismet += $form['#akismet'];
    }

    // Check server connectivity. Show a message when we had a problem (and
    // our chosen fallback method does not allow new submissions)..
    $status = AkismetUtilities::getApiKeyStatus();

    if (!$status['isVerified']) {
      AkismetUtilities::handleFallback($form_state);
    }

    // Setup initial Akismet session and form information.
    $akismet += [
      // Only TRUE if the form is protected by text analysis.
      'require_analysis' => $element['#akismet_form']['mode'] == FormInterface::AKISMET_MODE_ANALYSIS,
      // Becomes TRUE if the form is protected by text analysis and the
      // submitted entity should be unpublished.
      'require_moderation' => FALSE,
      // Internally used bag for last Akismet API responses.
      'response' => [],
    ];

    $akismet_form_array = $element['#akismet_form'];
    $akismet += $akismet_form_array;

    // By default, bad form submissions are discarded, unless the form was
    // configured to moderate bad posts. 'discard' may only be FALSE, if there
    // is a valid 'moderation callback'. Otherwise, it must be TRUE.
    if (empty($akismet['moderation callback']) || !function_exists($akismet['moderation callback'])) {
      $akismet['discard'] = TRUE;
    }

    $form_state->setValue('akismet', $akismet);

    // Add the Akismet session data elements.
    // These elements resemble the {akismet} database schema. The form
    // validation handlers will pollute them with values returned by Akismet.
    // For entity forms, the submitted values will appear in a $entity->akismet
    // property, which in turn represents the Akismet session data record to be
    // stored.
    $element['entity'] = [
      '#type' => 'value',
      '#value' => isset($akismet['entity']) ? $akismet['entity'] : 'akismet_content',
    ];
    $element['id'] = [
      '#type' => 'value',
      '#value' => NULL,
    ];
    $element['form_id'] = [
      '#type' => 'value',
      '#value' => $akismet['id'],
    ];
    $element['moderate'] = [
      '#type' => 'value',
      '#value' => 0,
    ];
    $element['classification'] = [
      '#type' => 'value',
      '#value' => NULL,
    ];
    $element['passed_validation'] = [
      '#type' => 'value',
      '#value' => FALSE,
    ];

    // Add link to privacy policy on forms protected via textual analysis,
    // if enabled.
    if ($akismet_form_array['mode'] == FormInterface::AKISMET_MODE_ANALYSIS && \Drupal::config('akismet.settings')->get('privacy_link')) {
      $element['privacy'] = [
        '#prefix' => '<div class="description akismet-privacy">',
        '#suffix' => '</div>',
        '#markup' => t('By submitting this form, you accept the <a href="@privacy-policy-url" class="akismet-target" rel="nofollow">Akismet privacy policy</a>.', [
          '@privacy-policy-url' => 'https://akismet.com/web-service-privacy-policy',
        ]),
        '#weight' => 10,
      ];
    }

    return $element;
  }

}
