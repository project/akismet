<?php

namespace Drupal\Tests\akismet\Functional;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\akismet\Entity\FormInterface;

/**
 * Tests toggling of testing mode.
 *
 * @group akismet
 */
class TestingModeTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'akismet_test',
  ];

  /**
   * Overrides AkismetWebTestCase::$akismetClass.
   *
   * @var string
   *
   * In order to test toggling of the testing mode, ensure the regular class for
   * production usage is used.
   */
  protected $akismetClass = 'AkismetDrupal';

  /**
   * Disable the default set up.
   *
   * @var bool
   */
  public $disableDefaultSetup = TRUE;

  /**
   * Place to store the module settings during testing.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->settings = \Drupal::configFactory()->getEditable('akismet.settings');
    $this->settings->set('test_mode.enabled', FALSE);
    $this->settings->set('api_key', '');
    $this->settings->save();

    $this->getClient(TRUE);

    // Enable testing mode warnings.
    \Drupal::state()->set('akismet.omit_warning', FALSE);

    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer akismet',
    ]);
  }

  /**
   * Tests enabling and disabling of testing mode.
   */
  public function testTestingMode() {
    $this->drupalLogin($this->adminUser);

    // Protect akismet_test_form.
    $this->setProtectionUi('akismet_test_post_form');
    $this->settings->set('fallback', FormInterface::AKISMET_FALLBACK_ACCEPT)->save();

    // Setup production API keys and expected languages. They must be retained.
    $key = 'the-invalid-akismet-api-key-value';
    $edit = [
      'api_key' => $key,
    ];
    $this->drupalGet('admin/config/content/akismet/settings');
    $this->assertSession()->pageTextContains('The Akismet API key is not configured yet.');
    $this->submitForm($edit, 'Save configuration');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextNotContains('The Akismet API key is not configured yet.');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->pageTextContains('The configured Akismet API key is invalid.');

    $this->drupalLogout();

    // Verify that spam can be posted, since testing mode is disabled and API
    // keys are invalid.
    $edit = [
      'title' => $this->randomString(),
      'body' => 'spam',
    ];
    $this->drupalGet('akismet-test/form');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->submitForm($edit, 'Save');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextContains('Successful form submission.');

    // Enable testing mode.
    $this->drupalLogin($this->adminUser);
    $edit = [
      'testing_mode' => TRUE,
    ];
    $this->drupalGet('admin/config/content/akismet/settings');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextContains('The configured Akismet API key is invalid.');
    $this->submitForm($edit, 'Save configuration');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextNotContains('The Akismet API key is not configured yet.');
    $this->assertSession()->pageTextNotContains('The configured Akismet API keys are invalid.');
    $this->assertSession()->pageTextContains('Akismet testing mode is still enabled.');

    // Make the key valid. Anticipate the error about the old key being invalid.
    $edit = [
      'api_key' => 'validkey',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);

    $this->drupalLogout();

    // Verify presence of testing mode warning.
    $this->drupalGet('akismet-test/form');

    // @todo There is a problem with the way #lazy_builder is handling the
    //   status messages in tests.  As a result, the text is only output when a
    //   message is set before it too.... further investigation is needed.
    //   See https://www.drupal.org/project/akismet/issues/3225042
    // $this->assertText(t('Akismet testing mode is still enabled.'));
    // Verify that no spam can be posted with testing mode enabled.
    // @todo Re-evaluate if this makes sense. Akismet testing mode doesn't
    //   change this behaviour, it just sends an additional input to the API.
    //   This part of the test succeeding is thanks to the local test server,
    //   not the testing mode. Maybe we should somehow verify the existence of
    //   the is_test input instead (but we should make sure we use the
    //   appropriate client, in that case.
    //   See https://www.drupal.org/project/akismet/issues/3225042
    $edit = [
      'title' => $this->randomString(),
      'body' => 'spam',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);
    $this->assertSession()->pageTextNotContains('Successful form submission.');

    // Disable testing mode.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/akismet/settings');
    $this->assertSession()->pageTextContains('Akismet testing mode is still enabled.');
    $edit = [
      'testing_mode' => FALSE,
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->pageTextNotContains('Akismet testing mode is still enabled.');

    // Verify that production API keys still exist.
    $this->assertSession()->fieldValueEquals('api_key', 'validkey');
  }

}
