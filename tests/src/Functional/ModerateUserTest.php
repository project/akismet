<?php

namespace Drupal\Tests\akismet\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\user\UserInterface;

/**
 * Tests moderating user accounts.
 *
 * @group akismet
 */
class ModerateUserTest extends AkismetTestBase {

  use AssertMailTrait;

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'akismet_test',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi(
      'user_register_form',
      [],
      ['discard' => 0]
    );
    $this->drupalLogout();

    // Allow visitors to register.
    // Disable e-mail verification.
    // Set default user account cancellation method.
    $config = $this->config('user.settings');
    $config
      ->set('verify_mail', FALSE)
      ->set('register', UserInterface::REGISTER_VISITORS)
      ->set('cancel_method', 'user_cancel_delete')
      ->save();
  }

  /**
   * Tests moderating users.
   */
  public function testModerateUser() {
    $account = $this->registerUser();
    $this->assertAkismetData('user', $account->id());

    // Log in administrator and verify that we can report to Akismet.
    $this->drupalLogin($this->adminUser);
    $edit = [
      'user_cancel_method' => 'user_cancel_delete',
      'akismet[feedback]' => 'spam',
    ];
    $this->drupalGet('user/' . $account->id() . '/cancel');
    $this->submitForm($edit, 'edit-submit');
    // @todo errrrr, "content"? ;)
    $this->assertSession()->pageTextContains('The content was successfully reported as inappropriate.');

    // Verify that Akismet session data has been deleted.
    $this->assertNoAkismetData('user', $account->id());
  }

  /**
   * Tests cancelling own user account.
   */
  public function testCancelOwnAccount() {
    // Allow users to cancel their own accounts.
    user_role_grant_permissions(AccountInterface::AUTHENTICATED_ROLE, ['cancel account']);

    // Register a non-spammy user.
    $account = $this->registerUser(FALSE);
    $this->assertAkismetData('user', $account->id());

    // Verify that no feedback options appear on the account cancellation form.
    $this->drupalGet('user/' . $account->id() . '/cancel');
    $this->assertSession()->pageTextNotContains('Report as');

    // Cancel own account.
    $this->submitForm([], 'edit-submit');
    $this->assertSession()->pageTextContains('A confirmation request to cancel your account has been sent to your email address.');
    $this->assertSession()->pageTextNotContains('The content was successfully reported as inappropriate.');

    // Confirm account cancellation request.
    // Turn off error assertion because the link returns a 404 due to the batch
    // user process... but it really does work to cancel the account. hmmmm.
    $this->assertWatchdogErrors = FALSE;
    $this->drupalGet($this->getCancelUrl());

    // Verify that Akismet session data has been deleted.
    $this->assertNoAkismetData('user', $account->id());
  }

  /**
   * Registers a new user through the UI.
   *
   * @param bool $spam
   *   Whether to register a spammy user or not. Defaults to TRUE.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   The new user account.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function registerUser(bool $spam = TRUE): AccountInterface {
    $password = $this->randomMachineName();
    $edit = [
      'name' => $this->randomMachineName(),
      'pass[pass1]' => $password,
      'pass[pass2]' => $password,
      'mail' => $spam ? 'spam@example.com' : 'ham@example.com',
    ];
    $this->drupalGet('user/register');
    $this->submitForm($edit, 'Create new account');

    // Determine new uid.
    $uids = \Drupal::entityQuery('user')->condition('name', $edit['name'])->accessCheck()->execute();
    /** @var \Drupal\Core\Session\AccountInterface $account */
    $account = \Drupal::entityTypeManager()->getStorage('user')->load(reset($uids));

    return $account;
  }

  /**
   * Parses the user account cancellation URL out of the last sent mail.
   */
  private function getCancelUrl() {
    $mails = $this->getMails();
    $mail = end($mails);
    preg_match('@[^\s]+user/\d+/cancel/confirm/[^\s]+@', $mail['body'], $matches);
    return $matches[0];
  }

}
