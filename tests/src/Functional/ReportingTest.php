<?php

namespace Drupal\Tests\akismet\Functional;

use Drupal\Core\Session\AccountInterface;

/**
 * Class ReportingTest.
 *
 * Verify that session data is properly stored and content can be reported to
 * Akismet.
 *
 * @group akismet
 */
class ReportingTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'akismet_test',
    'views',
  ];

  /**
   * User to run tests with.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $webUser;

  /**
   * Reference to created node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);
    $this->addCommentsToNode();

    $this->webUser = $this->drupalCreateUser([
      'create article content',
      'access comments',
      'post comments',
      'skip comment approval',
    ]);
  }

  /**
   * Tests reporting comments.
   */
  public function testReportComment() {
    $comment_storage = \Drupal::entityTypeManager()->getStorage('comment');
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('comment_comment_form');
    $this->drupalLogout();

    $this->node = $this->drupalCreateNode(['type' => 'article']);

    // Post a comment.
    $this->drupalLogin($this->webUser);
    $edit = [
      'comment_body[0][value]' => 'ham',
    ];
    $this->drupalGet('node/' . $this->node->id());
    $this->submitForm($edit, 'Save');
    $comments = $this->loadCommentsBySubject($edit['comment_body[0][value]']);
    $comment = $comment_storage->load(reset($comments));
    $this->assertIsObject($comment, 'Comment was found in the database.');
    $this->assertAkismetData('comment', $comment->id());

    // Log in comment administrator and verify that we can report to Akismet.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/' . $this->node->id());
    // Check that the comment is found.
    $this->assertSession()->pageTextContains($edit['comment_body[0][value]']);
    $this->clickLink('Delete');
    $edit = [
      'akismet[feedback]' => 'spam',
    ];
    $this->submitForm($edit, 'Delete');
    $this->assertSession()->pageTextContains('The comment and all its replies have been deleted.');
    $this->assertSession()->pageTextContains('The content was successfully reported as inappropriate.');

    // Verify that the comment and Akismet session data has been deleted.
    $comment_storage->resetCache();
    $this->assertTrue(empty($comment_storage->load($comment->id())), 'Comment was deleted.');
    $this->assertNoAkismetData('comment', $comment->id());
  }

  /**
   * Tests mass-reporting comments.
   */
  public function testMassReportComments() {
    /** @var \Drupal\comment\Entity\Comment[] $comments */
    $comments = [];
    $comment_storage = \Drupal::entityTypeManager()->getStorage('comment');
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('comment_comment_form');
    $this->drupalLogout();

    $this->node = $this->drupalCreateNode(['type' => 'article']);
    $this->addCommentsToNode();

    // Post 3 comments.
    $this->drupalLogin($this->webUser);
    foreach (range(1, 3) as $num) {
      $edit = [
        'subject[0][value]' => $this->randomMachineName(),
        'comment_body[0][value]' => 'ham',
      ];
      $this->drupalGet('node/' . $this->node->id());
      $this->submitForm($edit, 'Save');
      $cids = $this->loadCommentsBySubject($edit['subject[0][value]']);
      $comments[$num] = $comment_storage->load(reset($cids));
      $this->assertTrue(!empty($comments[$num]), 'Comment was found in the database.');
      $this->assertAkismetData('comment', $comments[$num]->id());
    }

    // Log in comment administrator and verify that we can mass-report all
    // comments to Akismet.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/content/comment');
    $edit = [
      'action' => 'comment_delete_action',
    ];
    $i = 0;
    foreach ($comments as $comment) {
      $this->assertSession()->pageTextContains($comment->getSubject());
      $edit["comment_bulk_form[$i]"] = TRUE;
      $i++;
    }
    $this->submitForm($edit, 'Apply to selected items');
    foreach ($comments as $comment) {
      $this->assertSession()->pageTextContains($comment->getSubject());
    }

    $edit = [
      // Disable reporting test.
      // @see https://www.drupal.org/project/akismet/issues/3209465
      // 'akismet[feedback]' => 'spam',
    ];
    $this->submitForm($edit, 'Delete');
    $this->assertSession()->pageTextContains('Deleted ' . count($comments) . ' comments.');
    // Disable assertion of report message.
    // @see https://www.drupal.org/project/akismet/issues/3209465
    // $this->assertText(t('The posts were successfully reported as inappropriate.'));

    // Verify that the comments and Akismet session data has been deleted.
    $comment_storage->resetCache();
    foreach ($comments as $comment) {
      $this->assertTrue(empty($comment_storage->load($comment->id())), 'Comment was deleted.');
      $this->assertNoAkismetData('comment', $comment->id());
    }
  }

  /**
   * Tests appearance of feedback options on node delete forms.
   */
  public function testReportNode() {
    // Create a second node type, which is not protected.
    $this->drupalCreateContentType([
      'type' => 'unprotected',
      'name' => 'Unprotected',
    ]);
    user_role_grant_permissions(AccountInterface::AUTHENTICATED_ROLE, [
      'create unprotected content',
      'delete own unprotected content',
      'delete own article content',
    ]);

    // Protect the article node type.
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('node_article_form');
    $this->drupalLogout();

    // Login and submit a protected article node.
    $this->drupalLogin($this->webUser);
    $this->drupalGet('node/add/article');
    $edit = [
      'title[0][value]' => 'protected ham',
      'body[0][value]' => 'ham',
    ];
    $this->submitForm($edit, 'Save');
    $this->node = $this->drupalGetNodeByTitle($edit['title[0][value]']);
    $this->assertSession()->addressEquals('node/' . $this->node->id());
    $this->assertAkismetData('node', $this->node->id());

    // Verify that no feedback options appear on the delete confirmation form
    // for the node author.
    $this->drupalGet('node/' . $this->node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextNotContains('Report as…');

    // Verify that feedback options appear for the admin user.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/' . $this->node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Report as…');

    // Login and submit an unprotected node.
    $this->drupalLogin($this->webUser);
    $this->drupalGet('node/add/unprotected');
    $edit = [
      'title[0][value]' => 'unprotected spam',
      'body[0][value]' => 'spam',
    ];
    $this->submitForm($edit, 'Save');
    $this->node = $this->drupalGetNodeByTitle($edit['title[0][value]']);
    $this->assertSession()->addressEquals('node/' . $this->node->id());
    $this->assertNoAkismetData('node', $this->node->id());

    // Verify that no feedback options appear on the delete confirmation form
    // for the node author.
    $this->drupalGet('node/' . $this->node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextNotContains('Report as…');

    // Verify that no feedback options appear for the admin user.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('node/' . $this->node->id() . '/delete');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextNotContains('Report as…');
  }

  /**
   * Tests mass-reporting nodes.
   */
  public function testMassReportNodes() {
    /** @var \Drupal\node\Entity\Node[] $nodes */
    $nodes = [];
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('node_article_form');
    $this->drupalLogout();

    // Post 3 nodes.
    $this->drupalLogin($this->webUser);
    foreach (range(0, 2) as $num) {
      $this->drupalGet('node/add/article');
      $edit = [
        'title[0][value]' => $this->randomMachineName(),
        'body[0][value]' => 'ham',
      ];
      $this->submitForm($edit, 'Save');
      $node = $this->drupalGetNodeByTitle($edit['title[0][value]']);
      $this->assertSession()->addressEquals('node/' . $node->id());
      $this->assertAkismetData('node', $node->id());
      $nodes[] = $node;
    }

    // Log in as administrator and verify that we can mass-report all
    // nodes to Akismet.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/content');
    $edit = [
      'action' => 'node_delete_action',
    ];
    $i = 0;
    foreach ($nodes as $node) {
      $this->assertSession()->pageTextContains($node->label());
      $edit["node_bulk_form[{$i}]"] = TRUE;
      $i++;
    }
    $this->submitForm($edit, 'Apply to selected items');
    foreach ($nodes as $node) {
      $this->assertSession()->pageTextContains($node->label());
    }
    $edit = [
      // Disable reporting test.
      // @see https://www.drupal.org/project/akismet/issues/3209465
      // 'akismet[feedback]' => 'spam',
    ];
    $this->submitForm($edit, 'Delete');
    $this->assertSession()->pageTextContains('Deleted ' . count($nodes) . ' content items.');
    // Disable reporting test.
    // @see https://www.drupal.org/project/akismet/issues/3209465
    // $this->assertText(t('The posts were successfully reported as inappropriate.'));

    // Verify that the comments and Akismet session data has been deleted.
    $node_storage->resetCache();
    foreach ($nodes as $node) {
      $this->assertTrue(empty($node_storage->load($node->id())), 'Node was deleted.');
      $this->assertNoAkismetData('node', $node->id());
    }
  }

}
