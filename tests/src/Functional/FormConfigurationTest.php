<?php

namespace Drupal\Tests\akismet\Functional;

use Drupal\akismet\Controller\FormController;
use Drupal\akismet\Entity\Form;

/**
 * Verify that forms can be properly protected and unprotected.
 *
 * @group akismet
 */
class FormConfigurationTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'akismet_test',
  ];

  /**
   * Use local mock server.
   *
   * @var bool
   */
  protected $useLocal = TRUE;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests configuration of form fields for textual analysis.
   */
  public function testFormFieldsConfiguration() {
    $form_info = FormController::getProtectedFormDetails('akismet_test_post_form', 'akismet_test');

    // Protect Akismet test form.
    $this->drupalGet('admin/config/content/akismet/add-form', ['query' => ['form_id' => 'akismet_test_post_form']]);
    $this->assertSession()->pageTextContains('Akismet test form');

    $edit = [
      'unsure' => 'discard',
      'discard' => 1,
      'enabled_fields[title]' => TRUE,
      'enabled_fields[body]' => TRUE,
      'enabled_fields[exclude]' => FALSE,
      'enabled_fields[' . rawurlencode('parent][child') . ']' => TRUE,
      'enabled_fields[field]' => TRUE,
    ];
    $this->submitForm($edit, 'Create Protected Akismet Form');

    // Verify that akismet_test_post_form form was protected.
    $this->assertSession()->pageTextContains('The form protection has been added.');
    $this->assertSession()->pageTextContains('Akismet test form');
    $akismet_form = $this->loadAkismetConfiguredForm('akismet_test_post_form');
    $this->assertIsObject($akismet_form, 'Form configuration exists.');

    // Verify that field configuration was properly stored.
    $this->drupalGet('admin/config/content/akismet/form/akismet_test_post_form/edit');
    foreach ($edit as $name => $value) {
      // Skip any inputs that are not the fields for analysis checkboxes.
      if (strpos($name, '[enabled_fields]') === FALSE) {
        continue;
      }
      // assertFieldByName() does not work for checkboxes.
      // @see assertFieldChecked()
      $elements = $this->xpath('//input[@name=:name]', [':name' => $name]);
      if (isset($elements[0])) {
        if ($value) {
          $this->assertTrue(!empty($elements[0]['checked']), 'Field @name is checked', ['@name' => $name]);
        }
        else {
          $this->assertTrue(empty($elements[0]['checked']), 'Field @name is not checked', ['@name' => $name]);
        }
      }
      else {
        $this->fail('Field @name not found.', ['@name' => $name]);
      }
    }

    // Remove the title field from those that were enabled.
    $test_enabled_fields = ['body', 'exclude', 'parent][child', 'field'];
    $akismet_form->setEnabledFields($test_enabled_fields)->save();

    // Try a submit of the form.
    $this->drupalLogout();
    $edit = [
      'title' => 'unsure',
      'body' => 'unsure',
    ];
    $this->drupalGet('akismet-test/form');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);

    $data = $this->getServerRecord();
    $this->assertTrue(empty($data['postTitle']), 'Post title was not passed to Akismet.');

    // Add the title back.
    $this->drupalLogin($this->adminUser);
    $test_enabled_fields[] = 'title';
    // Add a field to the stored configuration that existed previously.
    $test_enabled_fields[] = 'orphan_field';
    $akismet_form->setEnabledFields($test_enabled_fields)->save();

    // Verify that field configuration contains only available elements.
    $this->drupalGet('admin/config/content/akismet/form/akismet_test_post_form/edit');
    $fields = $this->xpath('//input[starts-with(@name, "enabled_fields")]');
    $elements = [];
    foreach ($fields as $field) {
      // Strip out 'enabled_fields[' from the start and ']' from the end.
      $elements[] = substr(substr(rawurldecode($field->getAttribute('name')), 0, -1), strlen('enabled_fields['));
    }
    $this->assertEquals($elements, array_keys($form_info['elements']), 'Field list only contains available form elements.');

    // Try a simple submit of the form.
    $this->drupalLogout();
    $edit = [
      'title' => 'unsure',
    ];
    $this->drupalGet('akismet-test/form');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextNotContains('Successful form submission.');
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);
    $this->submitForm([], 'Save');

    // Try to submit values for top-level fields.
    $edit = [
      'title' => 'spam',
      'body' => 'spam',
    ];
    $this->drupalGet('akismet-test/form');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextNotContains('Successful form submission.');
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);

    // Try to submit values for nested field.
    $edit = [
      'title' => $this->randomString(),
      'parent[child]' => 'spam',
    ];
    $this->drupalGet('akismet-test/form');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextNotContains('Successful form submission.');
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);

    // Try to submit values for nested field and multiple value field.
    // Start with ham values for simple, nested, and first multiple field.
    $edit = [
      'title' => 'ham',
      'parent[child]' => 'ham',
      'field[new]' => 'ham',
    ];
    $this->drupalGet('akismet-test/form');
    $this->submitForm($edit, 'Add');

    // Verify that the form was rebuilt.
    $this->assertSession()->pageTextNotContains('Successful form submission.');
    $this->assertSession()->pageTextNotContains(self::SPAM_MESSAGE);

    // Add another value for multiple field.
    $edit = [
      'field[new]' => 'ham',
    ];
    $this->submitForm($edit, 'Add');

    // Verify that the form was rebuilt.
    $this->assertSession()->pageTextNotContains('Successful form submission.');
    $this->assertSession()->pageTextNotContains(self::SPAM_MESSAGE);

    // Now replace all ham values with random values, add a spam value to the
    // multiple field and submit the form.
    $edit = [
      'title' => $this->randomString(),
      'parent[child]' => $this->randomString(),
      'field[0]' => $this->randomString(),
      'field[1]' => $this->randomString(),
      'field[new]' => 'spam',
    ];
    $this->submitForm($edit, 'Save');

    // Verify that the form was not submitted and cannot be submitted.
    $this->assertSession()->pageTextNotContains('Successful form submission.');
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);

    // Verify that we can remove the form protection.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/akismet');
    $this->assertSession()->pageTextContains('Akismet test form');
    $this->drupalGet('admin/config/content/akismet/form/akismet_test_post_form/delete');

    $this->submitForm([], 'Remove Akismet Protection');
    $this->assertSession()->addressEquals('admin/config/content/akismet');
    $this->assertSession()->pageTextNotContains('Akismet test form');
    $this->assertSession()->pageTextContains('The form protection has been removed.');
    $akismet_form = $this->loadAkismetConfiguredForm('akismet_test_post_form');
    $this->assertNull($akismet_form, 'Form protection should not be found.');

    // Verify that the form is no longer protected.
    $this->drupalLogout();
    $edit = [
      'title' => 'unsure',
    ];
    $this->drupalGet('akismet-test/form');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('Successful form submission.');
    $this->assertSession()->pageTextNotContains(self::SPAM_MESSAGE);
  }

  /**
   * Test that correct fields are extracted for a form.
   */
  public function testFormProtectableFields() {
    $form_info = [];
    FormController::addProtectableFields($form_info, 'akismet_test_post', 'akismet_test_post');
    $expected = [
      'title',
      'body',
    ];
    $unexpected = [
      'mid',
      'uuid',
      'status',
      'readonly',
      'computed',
    ];
    foreach ($expected as $field) {
      $this->assertTrue(isset($form_info['elements'][$field]));
    }
    foreach ($unexpected as $field) {
      $this->assertFalse(isset($form_info['elements'][$field]));
    }
    $this->assertEquals($form_info['mapping']['post_id'], 'mid');
  }

  /**
   * Tests default configuration, protecting, and unprotecting forms.
   */
  public function testFormAdministration() {
    $form_info = FormController::getProtectableForms();
    foreach ($form_info as $form_id => $info) {
      $form_info[$form_id] += FormController::getProtectedFormDetails($form_id, $info['module']);
    }

    // Verify that user registration form is not protected.
    $this->drupalGet('admin/config/content/akismet');
    $this->assertSession()->pageTextNotContains($form_info['user_register_form']['title']);
    $this->assertNull($this->loadAkismetConfiguredForm('user_register_form'), 'Form configuration should not exist.');

    // Re-protect user registration form.
    $this->drupalGet('admin/config/content/akismet/add-form');
    $this->assertSession()->pageTextNotContains('All available forms are protected already.');
    $this->drupalGet('admin/config/content/akismet/add-form', ['query' => ['form_id' => 'user_register_form']]);
    $this->assertSession()->pageTextContains($form_info['user_register_form']['title']);
    $this->submitForm([], 'Create Protected Akismet Form');

    // Verify that user registration form was protected.
    $this->assertSession()->pageTextContains('The form protection has been added.');
    $this->assertSession()->pageTextContains($form_info['user_register_form']['title']);
    $this->assertIsObject($this->loadAkismetConfiguredForm('user_register_form'), 'Form configuration exists.');

    // Retrieve a list of all permissions to verify them below.
    $all_permissions = \Drupal::service('user.permissions')->getPermissions();

    // Iterate over all unconfigured forms and protect them.
    foreach ($form_info as $form_id => $info) {
      if (!$this->loadAkismetConfiguredForm($form_id)) {
        $this->drupalGet('admin/config/content/akismet/add-form', ['query' => ['form_id' => $form_id]]);
        $this->assertSession()->pageTextContains($info['title']);
        // Verify that forms specifying elements have all possible elements
        // preselected for textual analysis.
        $edit = [];
        if (!empty($info['elements'])) {
          foreach ($info['elements'] as $field => $label) {
            $field = rawurlencode($field);
            $this->assertSession()->fieldValueEquals("enabled_fields[$field]", $field);
          }
        }
        // Verify that CAPTCHA-only forms contain no configurable fields.
        else {
          $this->assertSession()->pageTextNotContains('Analyze text for');
          $this->assertSession()->pageTextNotContains('Text fields to analyze');
        }
        // Verify that bypass permissions are output.
        $this->assertSession()->responseContains($all_permissions['bypass akismet protection']['title']);
        foreach ($info['bypass access'] as $permission) {
          $this->assertSession()->responseContains($all_permissions[$permission]['title']);
        }
        $this->submitForm($edit, 'Create Protected Akismet Form');
        $this->assertSession()->pageTextContains('The form protection has been added.');
      }
    }

    // Verify that trying to add a form redirects to the overview.
    $this->drupalGet('admin/config/content/akismet/add-form');
    $this->assertSession()->pageTextContains('All available forms are protected already.');
    $this->assertSession()->addressEquals('admin/config/content/akismet');
  }

  /**
   * Tests invalid (stale) form configurations.
   */
  public function testInvalidForms() {
    $forms = [
      'nonexisting' => 'nonexisting_form',
      'user' => 'user_nonexisting_form',
      'node' => 'nonexisting_node_form',
      'comment' => 'comment_node_nonexisting_form',
    ];
    $mode = 0;
    foreach ($forms as $module => $form_id) {
      $akismet_form = FormController::getProtectedFormDetails($form_id, $module, []);
      $akismet_form['mode'] = $mode++;
      $form = Form::create($akismet_form);
      $form->id = $form_id;
      $form->save();
    }

    // Just visiting the form administration page is sufficient; it will throw
    // fatal errors, warnings, and notices.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/akismet');

    // Ensure that unprotecting the forms does not throw any notices either.
    foreach ($forms as $form_id) {
      $this->assertSession()->linkByHrefNotExists('admin/config/content/akismet/form/' . $form_id . '/edit');
      $this->assertSession()->linkByHrefExists('admin/config/content/akismet/form/' . $form_id . '/delete');
      $this->drupalGet('admin/config/content/akismet/form/' . $form_id . '/delete');
      $this->submitForm([], 'Remove Akismet Protection');
      $this->assertSession()->linkByHrefNotExists('admin/config/content/akismet/form/' . $form_id . '/delete');
    }
    // Confirm deletion.
    $configured = \Drupal::entityTypeManager()->getStorage('akismet_form')->loadMultiple();
    $this->assertEmpty($configured, 'No forms found.');
  }

  /**
   * Tests programmatically, conditionally disabling Akismet.
   */
  public function testFormAlter() {
    // Enable CAPTCHA-only protection for request user password form.
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('user_pass');
    $this->drupalLogout();

    // Conditionally disable protection and verify again.
    \Drupal::state()->set('akismet_test.disable_akismet', TRUE);
    $this->drupalGet('');
    $this->drupalGet('user/password');
  }

  /**
   * Helper function to load the Akismet configuration for a protected form.
   *
   * @param string $id
   *   The form ID to load protection details for.
   *
   * @return \Drupal\akismet\Entity\FormInterface|null
   *   The form configuration or NULL if it does not exist.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadAkismetConfiguredForm($id) {
    return \Drupal::entityTypeManager()->getStorage('akismet_form')->load($id);
  }

}
