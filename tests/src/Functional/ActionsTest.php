<?php

namespace Drupal\Tests\akismet\Functional;

use Drupal\akismet\Storage\ResponseDataStorage;
use Drupal\system\Entity\Action;

/**
 * Tests actions provided for comments and nodes.
 *
 * @group akismet
 */
class ActionsTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'views',
  ];

  /**
   * Regular user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);
    $this->addCommentsToNode();
    $this->webUser = $this->drupalCreateUser([
      'create article content',
      'access comments',
      'post comments',
      'skip comment approval',
    ]);

    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('node_article_form');
    $this->setProtectionUi('comment_comment_form');
    $this->drupalLogout();

    // Login and submit a few nodes.
    $this->nodes = [];
    $this->comments = [];
    $this->drupalLogin($this->webUser);
    for ($i = 0; $i < 2; $i++) {
      // Create a test node.
      $edit = [
        'title[0][value]' => 'ham node ' . $i,
      ];
      $this->drupalGet('node/add/article');
      $this->submitForm($edit, 'Save');
      $node = $this->drupalGetNodeByTitle($edit['title[0][value]']);

      $this->drupalGet('node/' . $node->id());
      $edit = [
        'comment_body[0][value]' => 'ham',
      ];
      $this->submitForm($edit, 'Save');
    }
  }

  /**
   * Test the akismet action on comments.
   *
   * Test that calling the akismet action function triggers the unpublish of
   * comments and marking as spam.
   */
  public function testCommentActions() {
    // @see https://www.drupal.org/project/akismet/issues/3209465.
    $this->markTestSkipped('Reporting spam is currently broken.');

    $comment_storage = \Drupal::entityTypeManager()->getStorage('comment');
    // Load the comment entity objects.

    /** @var \Drupal\comment\CommentInterface[] $comments */
    $comments = $comment_storage->loadMultiple();
    $contentIds = [];
    foreach ($comments as $comment) {
      $data = ResponseDataStorage::loadByEntity('comment', $comment->id());
      $contentIds[] = $data->contentId;
      $this->assertTrue($comment->isPublished(), 'Initial comment is published');
    }
    $action = Action::load('akismet_comment_unpublish_action');
    $action->execute($comments);

    $this->resetAll();
    foreach ($comments as $comment) {
      $this->assertFalse($comment->isPublished(), 'Comment is now unpublished');
      $server = $this->getServerRecord('feedback');
      $this->assertTrue(in_array($server['contentId'], $contentIds));
      $this->assertEquals($server['source'], 'akismet_action_unpublish_comment');
      $this->assertEquals($server['reason'], 'spam');
      $this->assertEquals($server['type'], 'moderate');
    }
  }

  /**
   * Test the Akismet action on nodes.
   *
   * Test that calling the akismet action function triggers the unpublish of
   * nodes and marking as spam.
   */
  public function testNodeActions() {
    // @see https://www.drupal.org/project/akismet/issues/3209465.
    $this->markTestSkipped('Reporting spam is currently broken.');

    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/content');
    $this->assertSession()->optionExists('edit-action', 'akismet_node_unpublish_action');

    $contentIds = [];
    /** @var \Drupal\node\NodeInterface[] $nodes */
    $nodes = $node_storage->loadMultiple();
    $i = 0;
    $edit = ['action' => 'akismet_node_unpublish_action'];
    foreach ($nodes as $node) {
      $edit['node_bulk_form[' . $i . ']'] = TRUE;
      $this->assertTrue($node->isPublished(), 'Initial node is published.');
      $data = ResponseDataStorage::loadByEntity('node', $node->id());
      $contentIds[] = $data->contentId;
      $i++;
    }

    list(, $minor_version) = explode('.', \Drupal::VERSION);
    $button_name = $minor_version < 2 ? 'Apply' : 'Apply to selected items';
    $this->submitForm($edit, $button_name);

    // Verify that all nodes are now unpublished.
    $node_storage->resetCache();
    $nodes = $node_storage->loadMultiple();
    foreach ($nodes as $node) {
      $this->assertFalse($node->isPublished(), 'Node is now unpublished.');
      $server = $this->getServerRecord('feedback');
      $this->assertTrue(in_array($server['contentId'], $contentIds));
      $this->assertEquals($server['source'], 'akismet_action_unpublish_node');
      $this->assertEquals($server['reason'], 'spam');
      $this->assertEquals($server['type'], 'moderate');
    }
  }

}
