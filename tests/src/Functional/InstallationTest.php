<?php

namespace Drupal\Tests\akismet\Functional;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\user\RoleInterface;

/**
 * Tests module installation and key error handling.
 *
 * @group akismet
 */
class InstallationTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'node',
    'comment',
  ];

  /**
   * Use local mock server.
   *
   * @var bool
   */
  protected $useLocal = TRUE;

  /**
   * Disable the default set up.
   *
   * @var bool
   */
  public $disableDefaultSetup = TRUE;

  /**
   * Do not create keys in the base setup.
   *
   * @var bool
   */
  protected $createKeys = FALSE;

  /**
   * Do not set up Akismet.
   *
   * @var bool
   */
  protected $setupAkismet = FALSE;

  /**
   * User to run tests with.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $webUser = NULL;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer site configuration',
      'administer modules',
      'administer permissions',
    ]);
    $this->webUser = $this->drupalCreateUser([]);
  }

  /**
   * Tests status handling after installation.
   *
   * We walk through a regular installation of the Akismet module instead of
   * using setUp() to ensure that everything works as expected.
   *
   * Note: Partial error messages tested here; hence, no t().
   */
  public function testInstallationProcess() {
    $message_short_invalid = 'The configured Akismet API key is invalid.';
    $message_invalid = 'The API key could not be verified.';
    $message_valid = 'Akismet verified your key. The service is operating correctly.';
    $message_missing = 'The Akismet API key is not configured yet.';
    $message_server = 'The Akismet server could not be contacted. Please make sure that your web server can make outgoing HTTP requests.';
    $message_saved = 'The configuration options have been saved.';
    $admin_message = 'Visit the Akismet settings page to add your API key.';
    $install_message = 'Akismet installed successfully. Visit the ' . 'Akismet settings page' . ' to add your API key.';

    $this->drupalLogin($this->adminUser);

    // Ensure there is no requirements error by default.
    $this->drupalGet('admin/reports/status');
    $this->clickLink('Run cron');
    $this->drupalGet('admin/modules');

    // Install the Akismet module.
    $this->submitForm(['modules[akismet][enable]' => TRUE], 'Install');
    $this->assertSession()->pageTextContains($install_message);

    // Now we can add the test module for the rest of the form tests.
    \Drupal::service('module_installer')->install([
      'akismet_test',
      'akismet_test_server',
    ]);
    $settings = \Drupal::configFactory()->getEditable('akismet.settings');
    $settings->set('log_level', RfcLogLevel::DEBUG);
    $settings->save();

    $this->resetAll();

    // Verify that forms can be submitted without valid Akismet module
    // configuration.
    $this->drupalLogin($this->webUser);
    $edit = [
      'title' => 'spam',
    ];
    $this->drupalGet('akismet-test/form');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('Successful form submission.');

    // Assign the 'administer akismet' permission and log in a user.
    $this->drupalLogin($this->adminUser);
    $edit = [
      RoleInterface::AUTHENTICATED_ID . '[administer akismet]' => TRUE,
    ];
    $this->drupalGet('admin/people/permissions');
    $this->submitForm($edit, 'Save permissions');

    // Verify presence of 'empty keys' error message.
    $this->drupalGet('admin/config/content/akismet');
    $this->assertSession()->pageTextContains($message_missing);
    $this->assertSession()->pageTextNotContains($message_invalid);

    // Verify requirements error about missing API keys.
    $this->drupalGet('admin/reports/status');
    $this->assertSession()->pageTextContains($message_missing);
    $this->assertSession()->pageTextContains($admin_message);

    // Configure invalid keys.
    $edit = [
      'api_key' => 'the-invalid-akismet-api-key-value',
    ];
    $this->drupalGet('admin/config/content/akismet/settings');
    $this->submitForm($edit, 'Save configuration');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextContains($message_saved);
    $this->assertSession()->pageTextNotContains(self::FALLBACK_MESSAGE);

    // Verify presence of 'incorrect keys' error message.
    $this->assertSession()->pageTextContains($message_short_invalid);
    $this->assertSession()->pageTextNotContains($message_missing);

    // Verify requirements error about invalid API keys.
    $this->drupalGet('admin/reports/status');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextContains($message_short_invalid);

    // Ensure unreachable servers.
    \Drupal::state()->set('akismet.testing_use_local_invalid', TRUE);

    // Verify presence of 'network error' message.
    $this->drupalGet('admin/config/content/akismet/settings');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextContains($message_server);
    $this->assertSession()->pageTextNotContains($message_missing);
    $this->assertSession()->pageTextNotContains($message_invalid);

    // Verify requirements error about network error.
    $this->drupalGet('admin/reports/status');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextContains($message_server);
    $this->assertSession()->pageTextNotContains(self::FALLBACK_MESSAGE);

    // From here on out the watchdog errors are just a nuisance.
    $this->assertWatchdogErrors = FALSE;

    // Prepare for using the valid test client.
    \Drupal::state()->setMultiple([
      'akismet.testing_use_local' => TRUE,
      'akismet.testing_use_local_invalid' => FALSE,
    ]);
    $this->resetAll();

    $this->getClient(TRUE);

    $edit = [
      'api_key' => 'validkey',
    ];
    $this->drupalGet('admin/config/content/akismet/settings');
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains($message_saved);
    $this->assertSession()->pageTextContains($message_valid);
    $this->assertSession()->pageTextNotContains($message_missing);
    $this->assertSession()->pageTextNotContains($message_invalid);

    // Verify that deleting keys throws the correct error message again.
    $this->drupalGet('admin/config/content/akismet/settings');
    $this->assertSession()->pageTextContains($message_valid);
    $edit = [
      'api_key' => '',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains($message_saved);
    // The module incorrectly shows this message in this condition. See
    // https://www.drupal.org/project/akismet/issues/3210899
    // $this->assertNoText($message_valid);
    $this->assertSession()->pageTextContains($message_missing);
    $this->assertSession()->pageTextNotContains($message_invalid);
  }

}
