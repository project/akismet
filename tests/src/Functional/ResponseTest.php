<?php

namespace Drupal\Tests\akismet\Functional;

use Akismet\Client\Client;

/**
 * Tests that Akismet server responses match expectations.
 *
 * @group akismet
 */
class ResponseTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'akismet_test',
  ];

  /**
   * Disable the default set up.
   *
   * @var bool
   */
  public $disableDefaultSetup = TRUE;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser();
  }

  /**
   * Tests akismet.checkContent().
   */
  public function testCheckContent() {
    $akismet = $this->getClient();
    $data = [
      'comment_author' => $this->adminUser->getAccountName(),
      'comment_author_email' => $this->adminUser->getEmail(),
      'user_ip' => \Drupal::request()->getClientIp(),
    ];

    // Ensure proper response for 'ham' submissions.
    $data['comment_content'] = 'ham';
    $result = $akismet->checkContent($data);
    $this->assertAkismetWatchdogMessages();
    $this->assertEqualWithMessage($result['classification'], 'ham', 'classification');
    $this->assertNotEmpty($result['guid'], 'Ham result should contain a GUID.');

    // Ensure proper response for 'spam' submissions, re-using session_id.
    $data['comment_content'] = 'unsure';
    $result = $akismet->checkContent($data);
    $this->assertAkismetWatchdogMessages();
    $this->assertEqualWithMessage($result['classification'], 'unsure',
      'classification');
    $this->assertNotEmpty($result['guid'], 'Unsure result should contain a GUID.');

    $data['comment_content'] = 'spam';
    $result = $akismet->checkContent($data);
    $this->assertAkismetWatchdogMessages();
    $this->assertEqualWithMessage($result['classification'], 'spam',
      'classification');
    $this->assertNotEmpty($result['guid'], 'Spam result should contain a GUID.');
  }

  /**
   * Test results of akismet.checkContent() across requests for a session.
   */
  public function testCheckContentSession() {
    $this->markTestSkipped('Akismet does not really support sessions(?). Maybe we should remove this test. Consider to test whether GUIDs are correctly passed around when reporting content.');
    $akismet = $this->getClient();
    $base_data = [
      'authorName' => $this->adminUser->getAccountName(),
      'authorMail' => $this->adminUser->getEmail(),
      'authorId' => $this->adminUser->id(),
      'authorIp' => \Drupal::request()->getClientIp(),
    ];

    // Sequence:
    // - Post unsure content
    // - Post spam content
    // - Expect spamClassification 'spam' (spam always trumps)
    $this->resetResponseID();
    $content_data = $base_data;
    $content_data['postBody'] = 'unsure';
    $result = $akismet->checkContent($content_data);
    $this->assertAkismetWatchdogMessages();
    $this->assertEqualWithMessage($result['spamScore'], 0.5, 'spamScore');
    $this->assertEqualWithMessage($result['spamClassification'], 'unsure',
      'spamClassification');
    $contentId = $this->assertResponseID('contentId', $result['id']);
    $content_data['id'] = &$contentId;

    $content_data['postBody'] = 'spam';
    $result = $akismet->checkContent($content_data);
    $this->assertAkismetWatchdogMessages();
    $this->assertEqualWithMessage($result['spamScore'], 1.0, 'spamScore');
    $this->assertEqualWithMessage($result['spamClassification'], 'spam',
      'spamClassification');
    $contentId = $this->assertResponseID('contentId', $result['id']);

    // @todo Enable following sequence after fixing Testing API.
    return;

    // Sequence:
    // - Post unsure content
    // - Post unsure content
    // - Expect spamClassification 'ham'
    // - Post ham content
    // - Expect spamClassification 'ham'
    // - Post unsure content
    // - Expect spamClassification 'ham'.
    $this->resetResponseID();
    $content_data = $base_data;
    $content_data['postBody'] = 'unsure';
    $result = $akismet->checkContent($content_data);
    $this->assertAkismetWatchdogMessages();
    $this->assertEqualWithMessage($result['spamScore'], 0.5, 'spamScore');
    $this->assertEqualWithMessage($result['spamClassification'], 'unsure',
      'spamClassification');
    $contentId = $this->assertResponseID('contentId', $result['id']);
    $content_data['id'] = &$contentId;

    $content_data['postBody'] = 'ham';
    $result = $akismet->checkContent($content_data);
    $this->assertAkismetWatchdogMessages();
    //$this->assertSame('spamScore', $result['spamScore'], 0.0);
    $this->assertEqualWithMessage($result['spamClassification'], 'ham', 'spamClassification');
    $contentId = $this->assertResponseID('contentId', $result['id']);
  }

}
