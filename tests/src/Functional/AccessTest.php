<?php

namespace Drupal\Tests\akismet\Functional;

use Drupal\Core\Logger\RfcLogLevel;

/**
 * Class AccessTest.
 *
 * Confirm that there is a working key pair and that this status is correctly
 * indicated on the module settings page for appropriate users.
 *
 * @group akismet
 */
class AccessTest extends AkismetTestBase {

  const MESSAGE_SAVED = 'The configuration options have been saved.';
  const MESSAGE_INVALID = 'The configured Akismet API key is invalid.';
  const MESSAGE_NOT_CONFIGURED = 'The Akismet API key is not configured yet.';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
  ];

  /**
   * Do not create keys.
   *
   * @var bool
   */
  protected $createKeys = FALSE;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $settings = \Drupal::configFactory()->getEditable('akismet.settings');
    $settings->set('test_mode.enabled', FALSE);
    $settings->set('api_key', '');
    $settings->save();
  }

  /**
   * Configure an invalid key pair and ensure error message.
   */
  public function testApiKey() {
    // No error message or watchdog messages should be thrown with default
    // testing keys.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/akismet/settings');

    $this->assertSession()->pageTextContains(self::MESSAGE_NOT_CONFIGURED);
    $this->assertSession()->pageTextNotContains(self::MESSAGE_SAVED);
    $this->assertSession()->pageTextNotContains(self::MESSAGE_INVALID);

    // Set up an invalid test key and check that an error message is shown.
    $edit = [
      'api_key' => 'foo',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextContains(self::MESSAGE_SAVED);
    $this->assertSession()->pageTextContains(self::MESSAGE_INVALID);
    $this->assertSession()->pageTextNotContains(self::MESSAGE_NOT_CONFIGURED);
  }

  /**
   * Test admin access to the settings page.
   *
   * Make sure that the Akismet settings page works for users with the
   * 'administer akismet' permission but not those without
   * it.
   */
  public function testAdminAccessRights() {
    // Check access for a user that only has access to the 'administer
    // site configuration' permission. This user should have access to
    // the Akismet settings page.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/akismet');
    $this->assertSession()->statusCodeEquals(200);

    // Check access for a user that has everything except the 'administer
    // akismet' permission. This user should not have access to the Akismet
    // settings page.
    $web_user = $this->drupalCreateUser(array_diff(\Drupal::moduleHandler()->invokeAll('perm'), ['administer akismet']));
    $this->drupalLogin($web_user);
    $this->drupalGet('admin/config/content/akismet');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::WARNING);
    $this->assertSession()->statusCodeEquals(403);
  }

}
