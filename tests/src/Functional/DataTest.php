<?php

namespace Drupal\Tests\akismet\Functional;

use Drupal\comment\CommentInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Session\AnonymousUserSession;
use Drupal\Core\Session\UserSession;
use Drupal\Core\Session\AccountInterface;
use Drupal\akismet\Controller\FormController;
use Drupal\akismet\Storage\ResponseDataStorage;

/**
 * Class DataTest.
 *
 * Verify that form registry information is properly transformed into data that
 * is sent to Akismet servers.
 *
 * @group akismet
 */
class DataTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'akismet_test',
  ];

  /**
   * Use local mock server.
   *
   * @var bool
   */
  protected $useLocal = TRUE;

  /**
   * Test akismet_form_get_values().
   */
  public function testFormGetValues() {
    // Load the context node.
    $node = $this->drupalCreateNode(['type' => 'page', 'promote' => 1]);

    // Form registry information.
    $form_info = [
      'elements' => [
        'subject' => 'Subject',
        'message' => 'Message',
        'parent][child' => 'Some nested element',
        'field_checked' => 'Field to check',
        'field_unchecked' => 'Field to ignore',
      ],
      'mapping' => [
        'comment_author' => 'name',
        'comment_author_email' => 'mail',
        'context_id' => 'nid',
      ],
    ];
    // Fields configured via Akismet admin UI based on $form_info['elements'].
    $fields = [
      'subject',
      'message',
      'parent][child',
      'field_checked',
    ];

    // Verify submitted form values for an anonymous/arbitrary user.
    \Drupal::getContainer()->set('current_user', new AnonymousUserSession());
    $values = [
      'subject' => 'Foo',
      'message' => 'Bar',
      'parent' => [
        'child' => 'Beer',
      ],
      'field_checked' => [
        'und' => [
          0 => ['value' => ''],
          1 => [
            'summary' => 'Check summary',
            'value' => 'Check first',
            'whatever' => 'whatever',
          ],
          2 => ['value' => 'Check second'],
          3 => ['nested_empty' => []],
          4 => ['nested_weird' => ['']],
        ],
      ],
      'field_unchecked' => [
        'und' => [
          0 => ['value' => 'Ignore first'],
          1 => ['value' => 'Ignore second'],
        ],
      ],
      'name' => 'Drupaler',
      'mail' => 'drupaler@example.com',
      'nid' => $node->id(),
    ];
    $form_state = new FormState();
    $form_state->setValues($values);
    $form_state->setButtons([]);
    $form_state->setValue(['akismet', 'context created callback'], 'node_akismet_context_created');
    $data = FormController::extractAkismetValues($form_state, $fields, $form_info['mapping']);
    $data += array_fill_keys([
      'post_id',
      'comment_content',
      'comment_author_id',
      'comment_author',
      'comment_author_email',
      'comment_author_url',
      'user_ip',
      'comment_post_modified_gmt',
    ], NULL);

    $body = [
      $values['subject'],
      $values['message'],
      $values['parent']['child'],
      $values['field_checked']['und'][1]['summary'],
      $values['field_checked']['und'][1]['value'],
      $values['field_checked']['und'][2]['value'],
    ];
    $this->assertEqualWithMessage($data['comment_content'], implode("\n", $body), 'comment_content');
    $this->assertEqualWithMessage($data['comment_author_id'], NULL, 'comment_author_id');
    $this->assertEqualWithMessage($data['comment_author'], $values['name'], 'comment_author');
    $this->assertEqualWithMessage($data['comment_author_email'], $values['mail'], 'comment_author_email');
    $this->assertEqualWithMessage($data['comment_author_url'], NULL, 'comment_author_url');
    $this->assertEqualWithMessage($data['user_ip'], \Drupal::request()->getClientIp(), 'user_ip');
    $this->assertEqualWithMessage($data['comment_post_modified_gmt'], $node->getCreatedTime(), 'comment_post_modified_gmt');

    // Verify submitted form values for an anonymous user whose name happens to
    // match a registered user.
    $values = [
      'subject' => 'Foo',
      'message' => 'Bar',
      'parent' => [
        'child' => 'Beer',
      ],
      'field_checked' => [
        0 => ['value' => 'Check first'],
        1 => ['value' => 'Check second'],
      ],
      'field_unchecked' => [
        0 => ['value' => 'Ignore first'],
        1 => ['value' => 'Ignore second'],
      ],
      'name' => $this->adminUser->getAccountName(),
      'nid' => $node->id(),
    ];
    $form_state = new FormState();
    $form_state->setValues($values);
    $form_state->setButtons([]);
    $form_state->setValue(['akismet', 'context created callback'], 'node_akismet_context_created');
    $data = FormController::extractAkismetValues($form_state, $fields, $form_info['mapping']);
    $data += array_fill_keys([
      'post_id',
      'comment_content',
      'comment_author_id',
      'comment_author',
      'comment_author_email',
      'comment_author_url',
      'user_ip',
      'comment_post_modified_gmt',
    ], NULL);

    $body = [
      $values['subject'],
      $values['message'],
      $values['parent']['child'],
      $values['field_checked'][0]['value'],
      $values['field_checked'][1]['value'],
    ];
    $this->assertEqualWithMessage($data['comment_content'], implode("\n", $body), 'comment_content');
    $this->assertEqualWithMessage($data['comment_author_id'], NULL, 'comment_author_id');
    $this->assertEqualWithMessage($data['comment_author'], $values['name'], 'comment_author');
    $this->assertEqualWithMessage($data['comment_author_email'], NULL, 'comment_author_email');
    $this->assertEqualWithMessage($data['comment_author_url'], NULL, 'comment_author_url');
    $this->assertEqualWithMessage($data['user_ip'], \Drupal::request()->getClientIp(), 'user_ip');
    $this->assertEqualWithMessage($data['comment_post_modified_gmt'], $node->getCreatedTime(), 'comment_post_modified_gmt');

    // Verify submitted form values for a registered user.
    $admin_session = new UserSession([
      'uid' => $this->adminUser->id(),
      'name' => $this->adminUser->getAccountName(),
      'mail' => $this->adminUser->getEmail(),
    ]);
    \Drupal::getContainer()->set('current_user', $admin_session);
    $values = [
      'subject' => 'Foo',
      'message' => 'Bar',
      'name' => $this->adminUser->getAccountName(),
      'nid' => $node->id(),
    ];
    $form_state = new FormState();
    $form_state->setValues($values);
    $form_state->setButtons([]);
    $form_state->setValue(['akismet', 'context created callback'], 'node_akismet_context_created');
    $data = FormController::extractAkismetValues($form_state, $fields, $form_info['mapping']);
    $data += array_fill_keys([
      'post_id',
      'comment_content',
      'comment_author_id',
      'comment_author',
      'comment_author_email',
      'comment_author_url',
      'user_ip',
      'comment_post_modified_gmt',
    ], NULL);

    $body = [
      $values['subject'],
      $values['message'],
    ];
    $this->assertEqualWithMessage($data['comment_content'], implode("\n", $body), 'comment_content');
    $this->assertEqualWithMessage($data['comment_author_id'], $this->adminUser->id(), 'comment_author_id');
    $this->assertEqualWithMessage($data['comment_author'], $this->adminUser->getAccountName(), 'comment_author');
    $this->assertEqualWithMessage($data['comment_author_email'], $this->adminUser->getEmail(), 'comment_author_email');
    $this->assertEqualWithMessage($data['comment_author_url'], NULL, 'comment_author_url');
    $this->assertEqualWithMessage($data['user_ip'], \Drupal::request()->getClientIp(), 'user_ip');
    $this->assertEqualWithMessage($data['comment_post_modified_gmt'], $node->getCreatedTime(), 'comment_post_modified_gmt');

    // Verify that invalid UTF-8 is detected.
    $values = [
      'subject' => "Foo \xC0 bar",
    ];
    $form_state = new FormState();
    $form_state->setValues($values);
    $form_state->setButtons([]);
    $data = FormController::extractAkismetValues($form_state, $fields, $form_info['mapping']);
    $this->assertFalse($data, 'Invalid UTF-8 detected.');
    $this->assertAkismetWatchdogMessages();
  }

  /**
   * Test that form button values are not contained in postBody sent to Akismet.
   */
  public function testFormButtonValues() {
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('akismet_test_post_form');
    $this->drupalLogout();

    // Verify that neither the "Submit" nor the "Add" button value is contained
    // in the post body.
    $edit = [
      'title' => 'ham',
      'body' => 'ham',
    ];
    $this->drupalGet('akismet-test/form');
    $this->submitForm($edit, 'Save');
    $data = $this->getServerRecord();
    $this->assertEquals(0, preg_match('@Submit|Add@', $data['comment_content']), 'Button values should not be found in post body.');
  }

  /**
   * Test submitted post and author information for textual analysis.
   */
  public function testAnalysis() {
    // Create Article node type.
    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);

    // Make comment preview optional and grant permissions.
    $this->addCommentsToNode('article');

    user_role_grant_permissions(AccountInterface::ANONYMOUS_ROLE, [
      'access comments',
      'post comments',
    ]);
    user_role_grant_permissions(AccountInterface::AUTHENTICATED_ROLE, [
      'access comments',
      'post comments',
      'skip comment approval',
    ]);
    // Add 'administer comments' permission to $adminUser.
    $this->addPermissionsToAdmin(['administer comments']);

    // Rebuild menu router, permissions, etc.
    // @todo Remove in D8.
    $this->resetAll();

    $this->drupalLogin($this->adminUser);
    $this->setProtection('comment_comment_form');

    // Create a node we can comment on.
    $node = $this->drupalCreateNode(['type' => 'article', 'promote' => 1]);
    $node_url = 'node/' . $node->id();
    $this->drupalGet($node_url);
    $this->assertSession()->pageTextContains($node->getTitle());
    $this->drupalLogout();

    // Log in regular user and post a comment.
    $web_user = $this->drupalCreateUser();
    $this->drupalLogin($web_user);
    $this->drupalGet($node_url);

    $edit = [
      'subject[0][value]' => $this->randomString(),
      'comment_body[0][value]' => 'unsure',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('Your comment has been queued for review by site administrators and will be published after approval.');

    // Verify that submitted data equals post data.
    $data = $this->getServerRecord();
    $this->assertEqualWithMessage(
      $data['comment_content'],
      implode("\n", [
        $edit['comment_body[0][value]'],
        $edit['subject[0][value]'],
      ]),
      'comment_content');
    $this->assertEqualWithMessage($data['comment_author'], $web_user->getAccountName(),
      'comment_author');
    $this->assertEqualWithMessage($data['comment_author_email'], $web_user->getEmail(), 'comment_author_email');
    $this->assertEqualWithMessage($data['comment_author_id'], $web_user->id(), 'comment_author_id');
    $cids = $this->loadCommentsBySubject($edit['subject[0][value]']);
    $this->assertTrue(!empty($cids), 'Comment exists in database.');

    // Allow anonymous users to post comments without approval.
    user_role_grant_permissions(AccountInterface::ANONYMOUS_ROLE, ['skip comment approval']);

    // Allow anonymous users to post contact information.
    $this->setCommentSettings('anonymous', CommentInterface::ANONYMOUS_MAY_CONTACT);

    // Log out and post a comment as anonymous user.
    $this->resetServerRecords();
    $this->drupalLogout();
    $this->drupalGet($node_url);

    // Ensure we have some potentially escaped characters in the values.
    $edit = [
      'name' => $this->randomString(6) . ' & ' . $this->randomString(),
      'subject[0][value]' => '"' . $this->randomString() . '"',
      'comment_body[0][value]' => 'unsure',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('Your comment has been queued for review by site administrators and will be published after approval.');

    // Verify that submitted data equals post data.
    $data = $this->getServerRecord();
    $this->assertEqualWithMessage(
      $data['comment_content'],
      implode("\n", [
        $edit['comment_body[0][value]'],
        $edit['subject[0][value]'],
      ]),
      'comment_content');
    $this->assertEqualWithMessage($data['comment_author'], $edit['name'], 'comment_author');
    $this->assertFalse(isset($data['comment_author_id']), 'comment_author_id: Undefined.');
    $cids = $this->loadCommentsBySubject($edit['subject[0][value]']);
    $this->assertTrue(!empty($cids), 'Comment exists in database.');

    // Log in admin user and edit comment containing spam.
    $this->resetServerRecords();
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('comment/' . reset($cids) . '/edit');
    // Post without modification.
    $this->submitForm([], 'Save');

    // Verify that no data was submitted to Akismet.
    $data = $this->getServerRecord();
    $this->assertNull($data, 'Administrative form submission was not validated by Akismet.');
  }

  /**
   * Tests automated 'post_id' mapping and session data storage.
   *
   * This is an atomic test to verify that a simple 'post_id' mapping defined
   * via hook_akismet_form_info() is sufficient for basic integration with
   * Akismet (without reporting).
   */
  public function testPostIdMapping() {
    // Enable protection for akismet_test_post_form.
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('akismet_test_post_form');
    $this->drupalLogout();

    // Submit a akismet_test thingy.
    $edit = [
      'title' => 'ham',
      'body' => $this->randomString(),
    ];
    $this->drupalGet('akismet-test/form');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('Successful form submission.');
    $mid = $this->getFieldValueByName('mid');
    $this->assertTrue($mid > 0, 'Submission was stored.');
    $data = $this->assertAkismetData('akismet_test_post', $mid);

    // Ensure we were redirected to the form for the stored entry.
    $this->assertSession()->fieldValueEquals('body', $edit['body']);
    $new_mid = $this->getFieldValueByName('mid');
    $this->assertEquals($new_mid, $mid, 'Existing entity id found.');

    // Verify that session data was stored.
    $this->assertEqualWithMessage($data->entity, 'akismet_test_post', 'entity');
    $this->assertEqualWithMessage($data->id, $mid, 'id');
    $this->assertEqualWithMessage($data->form_id, 'akismet_test_post_form', 'form_id');
    $stored = ResponseDataStorage::loadByEntity('akismet_test_post', $mid);
    $this->assertNotEmpty($stored, 'Data was stored in {akismet}.');

    // Update the stored entry.
    $edit['title'] = 'unsure';
    $this->submitForm($edit, 'Save');
    $new_data = $this->assertAkismetData('akismet_test_post', $mid);

    // Verify that only session data was updated.
    $this->assertEqualWithMessage($data->entity, $new_data->entity, 'entity');
    $this->assertEqualWithMessage($data->id, $new_data->id, 'id');
    $this->assertEqualWithMessage($data->form_id, $new_data->form_id, 'form_id');
    $this->assertEqualWithMessage($data->guid, $new_data->guid, 'guid');
    $stored = ResponseDataStorage::loadByEntity($data->entity, $data->id);
    $this->assertNotEmpty($stored, 'Stored data in {akismet} was updated.');
  }

  /**
   * Tests data sent for Akismet::verifyKeys().
   */
  public function testVerifyKey() {
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/config/content/akismet/settings');

    $randomKey = $this->randomMachineName();
    // Do not assert watchdog messages; the random key is almost certainly
    // invalid.
    $this->submitForm(['api_key' => $randomKey], 'Save configuration');
    $this->assertWatchdogErrors = FALSE;

    // Verify that we sent the new key to the server. We call verifyKeys often
    // so let's find the last record.
    while (!is_null($record = $this->getServerRecord('apikey'))) {
      $data = $record;
    }

    $this->assertEqualWithMessage($data['key'], $randomKey, 'api_key');
  }

}
