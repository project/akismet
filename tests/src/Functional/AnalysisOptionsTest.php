<?php

namespace Drupal\Tests\akismet\Functional;

use Drupal\Component\Render\FormattableMarkup;

/**
 * Tests text analysis options of binary mode, retaining unsure/spam.
 *
 * @group akismet
 */
class AnalysisOptionsTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'akismet_test',
  ];

  /**
   * Disable the default set up logic.
   *
   * @var bool
   */
  public $disableDefaultSetup = TRUE;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer akismet',
    ]);
  }

  /**
   * Tests binary unsure mode.
   */
  public function testUnsureBinary() {
    $this->markTestIncomplete('The binary option for unsure (publish the post when Akismet is unsure) is not currently supported, it is a remnant of the Mollom module. We may want to add it back later.');
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('akismet_test_post_form', NULL,
      ['unsure' => 'binary']);
    $this->drupalLogout();

    // Verify that an unsure post is ham, and that a ham post is accepted,
    // respectively.
    foreach (['unsure', ['ham']] as $body) {
      $edit = [
        'title' => $this->randomString(),
        'body' => $body,
      ];
      $this->assertHamSubmit('akismet-test/form', [], $edit, 'Save');
      $mid = $this->assertTestSubmitData();
      $data = $this->assertAkismetData('akismet_test_post', $mid);
      $record = $this->loadTestPost($mid);
      $this->assertEquals($record->getStatus(), 1, 'Published test post found.');
      $this->assertEqualWithMessage($data->spamScore, 0, 'spamScore');
      $this->assertEqualWithMessage($data->spamClassification, 'ham', 'spamClassification');
      $this->assertEqualWithMessage($data->moderate, 0, 'moderate');
    }

    // Verify that a spam post is blocked.
    $edit = [
      'title' => $this->randomString(),
      'body' => 'spam',
    ];
    $this->assertSpamSubmit('akismet-test/form', [], $edit, 'Save');
  }

  /**
   * Tests retaining unsure posts and moderating them.
   */
  public function testRetainUnsure() {
    $this->drupalLogin($this->adminUser);
    // Verify that akismet_basic_elements_test_form cannot be configured to put
    // posts into moderation queue.
    $this->setProtectionUi('akismet_basic_elements_test_form');
    $this->drupalGet('admin/config/content/akismet/form/akismet_basic_elements_test_form/edit');
    $this->assertSession()->fieldValueNotEquals('unsure', '');

    // Configure akismet_test_form to retain unsure posts.
    $this->setProtectionUi('akismet_test_post_form', NULL,
      ['unsure' => 'moderate']);
    $this->drupalLogout();

    // Verify that an unsure post gets unpublished.
    $edit = [
      'title' => $this->randomString(),
      'body' => 'unsure',
    ];
    $this->drupalGet('akismet-test/form');
    $this->submitForm($edit, 'Save');
    $mid = $this->assertTestSubmitData();
    $data = $this->assertAkismetData('akismet_test_post', $mid);
    $record = $this->loadTestPost($mid);
    $this->assertEquals($record->getStatus(), 0, 'Unpublished test post found.');
    $this->assertEqualWithMessage($data->classification, 'unsure', 'classification');
    $this->assertEqualWithMessage($data->moderate, 1, 'moderate');

    // Verify that editing the post does neither change the session data, nor
    // the publishing status.
    $edit = [
      'title' => $this->randomString(),
      'body' => 'unsure unsure',
    ];
    $this->submitForm($edit, 'Save');
    $mid = $this->assertTestSubmitData($mid);
    $data = $this->assertAkismetData('akismet_test_post', $mid);
    $record = $this->loadTestPost($mid);
    $this->assertEquals($record->getStatus(), 0, 'Unpublished test post found.');
    $this->assertEqualWithMessage($data->classification, 'unsure', 'classification');
    $this->assertEqualWithMessage($data->moderate, 1, 'moderate');

    // Verify that publishing the post changes the session data accordingly.
    $this->drupalLogin($this->adminUser);
    $edit = [
      'status' => TRUE,
    ];
    $this->drupalGet('akismet-test/form/' . $mid);
    $this->submitForm($edit, 'Save');
    $mid = $this->assertTestSubmitData($mid);
    $data = $this->assertAkismetData('akismet_test_post', $mid);
    $record = $this->loadTestPost($mid);
    $this->assertEquals($record->getStatus(), 1, 'Published test post found.');
    $this->assertEqualWithMessage($data->classification, 'unsure', 'classification');
    $this->assertEqualWithMessage($data->moderate, 0, 'moderate');
  }

  /**
   * Tests retaining spam posts and moderating them.
   */
  public function testRetainSpam() {
    $this->drupalLogin($this->adminUser);
    // Verify that akismet_basic_test_form cannot be configured to put posts
    // into moderation queue.
    $this->setProtectionUi('akismet_basic_elements_test_form');
    $this->drupalGet('admin/config/content/akismet/form/akismet_basic_elements_test_form/edit');
    $this->assertSession()->fieldValueNotEquals('discard', '');

    // Configure akismet_test_form to accept bad posts.
    $this->setProtectionUi('akismet_test_post_form', NULL, [
      'unsure' => 'moderate',
      'discard' => 0,
    ]);
    $this->drupalLogout();

    // Verify that we are able to post spam and the post is unpublished.
    $edit = [
      'title' => $this->randomString(),
      'body' => 'spam',
    ];
    $this->drupalGet('akismet-test/form');
    $this->submitForm($edit, 'Save');
    $mid = $this->assertTestSubmitData();
    $data = $this->assertAkismetData('akismet_test_post', $mid);
    $record = $this->loadTestPost($mid);
    $this->assertEquals($record->getStatus(), 0, 'Unpublished test post found.');
    $this->assertEqualWithMessage($data->classification, 'spam', 'classification');
    $this->assertEqualWithMessage($data->moderate, 1, 'moderate');

    // Verify that editing the post does neither change the session data, nor
    // the publishing status.
    $edit = [
      'title' => $this->randomString(),
      'body' => 'spam spam',
    ];
    $this->submitForm($edit, 'Save');
    $mid = $this->assertTestSubmitData($mid);
    $data = $this->assertAkismetData('akismet_test_post', $mid);
    $record = $this->loadTestPost($mid);
    $this->assertEquals($record->getStatus(), 0, 'Unpublished test post found.');
    $this->assertEqualWithMessage($data->classification, 'spam', 'classification');
    $this->assertEqualWithMessage($data->moderate, 1, 'moderate');

    // Verify that publishing the post changes the session data accordingly.
    $this->drupalLogin($this->adminUser);
    $edit = [
      'status' => TRUE,
    ];
    $this->drupalGet('akismet-test/form/' . $mid);
    $this->submitForm($edit, 'Save');
    $mid = $this->assertTestSubmitData($mid);
    $data = $this->assertAkismetData('akismet_test_post', $mid);
    $record = $this->loadTestPost($mid);
    $this->assertEquals($record->getStatus(), 1, 'Published test post found.');
    $this->assertEqualWithMessage($data->classification, 'spam', 'classification');
    $this->assertEqualWithMessage($data->moderate, 0, 'moderate');
  }

  /**
   * Tests that ham posts are not marked for moderation.
   */
  public function testPublishHam() {
    // No matter how we configure the protecteion, ham posts should be accepted.
    $unsureOptions = ['moderate', 'discard'];
    $discardOptions = [0, 1];

    foreach ($unsureOptions as $unsureOption) {
      foreach ($discardOptions as $discardOption) {
        $this->drupalLogin($this->adminUser);

        $this->setProtectionUi('akismet_test_post_form', NULL, [
          'unsure' => $unsureOption,
          'discard' => $discardOption,
        ]);
        $this->drupalLogout();

        $edit = [
          'title' => $this->randomString(),
          'body' => 'ham',
        ];
        $this->drupalGet('akismet-test/form');
        $this->submitForm($edit, 'Save');
        $mid = $this->assertTestSubmitData();
        $data = $this->assertAkismetData('akismet_test_post', $mid);
        $record = $this->loadTestPost($mid);
        $message = new FormattableMarkup(
          'Ham post must be accepted with unsure option set to @unsure_option and discard option set to @discard_option.',
          [
            '@unsure_option' => $unsureOption,
            '@discard_option' => $discardOption,
          ]);
        $this->assertEquals($record->getStatus(), 1, $message);
        $this->assertEqualWithMessage(
          $data->classification,
          'ham',
          'classification'
        );
        $this->assertEqualWithMessage($data->moderate, 0, 'moderate');
      }
    }
  }

  /**
   * Loads a test post entity.
   *
   * @param mixed $id
   *   The id to load.
   *
   * @return \Drupal\akismet_test\Entity\PostInterface
   *   The requested test post.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadTestPost($id) {
    $controller = \Drupal::entityTypeManager()->getStorage('akismet_test_post');
    $controller->resetCache([$id]);

    /** @var \Drupal\akismet_test\Entity\PostInterface $entity */
    $entity = $controller->load($id);
    return $entity;
  }

}
