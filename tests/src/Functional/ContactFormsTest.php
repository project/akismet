<?php

namespace Drupal\Tests\akismet\Functional;

/**
 * Tests protection of Contact module forms.
 *
 * @group akismet
 */
class ContactFormsTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'akismet_test',
    'contact',
    'field_ui',
    'text',
    'contact_test',
  ];

  /**
   * Disable the default set up.
   *
   * @var bool
   */
  public $disableDefaultSetup = TRUE;

  /**
   * User to run tests with.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $webUser;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'access administration pages',
      'administer akismet',
      'access site-wide contact form',
      'access user profiles',
      'access user contact forms',
      'administer contact forms',
      'administer users',
      'administer account settings',
      'administer contact_message fields',
    ]);

    $this->webUser = $this->drupalCreateUser([
      'access site-wide contact form',
      'access user profiles',
      'access user contact forms',
    ]);
  }

  /**
   * Make sure that the user contact form is protected correctly.
   */
  public function testProtectContactUserForm() {
    // Enable Akismet for the contact form.
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('contact_message_personal_form');
    $this->drupalLogout();

    $this->drupalLogin($this->webUser);
    $url = 'user/' . $this->adminUser->id() . '/contact';
    $button = 'Send message';
    $success = 'Your message has been sent.';

    // Submit a 'spam' message.  This should be blocked.
    $this->assertSpamSubmit($url, ['subject[0][value]', 'message[0][value]'], [], $button);
    $this->assertSession()->pageTextNotContains($success);

    // Submit a 'ham' message.  This should be accepted.
    $this->assertHamSubmit($url, ['subject[0][value]', 'message[0][value]'], [], $button);
    $this->assertSession()->pageTextContains($success);

    // Submit an 'unsure' message. This should not be accepted either.
    $this->assertUnsureSubmit($url, ['subject[0][value]', 'message[0][value]'], [], $button, $success);
  }

  /**
   * Make sure that the site-wide contact form is protected correctly.
   */
  public function testProtectContactSiteForm() {
    // Enable Akismet for the contact form.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('admin/structure/contact');
    // Default form exists.
    $this->assertSession()->linkByHrefExists('admin/structure/contact/manage/feedback/delete');

    $this->setProtectionUi('contact_message_feedback_form');
    $this->drupalGet('contact');
    $this->drupalLogout();

    $this->drupalLogin($this->webUser);
    $this->drupalGet('contact');
    $url = 'contact';
    $button = 'Send message';
    $success = 'Your message has been sent.';

    // Submit a 'spam' message.  This should be blocked.
    $this->assertSpamSubmit($url, ['subject[0][value]', 'message[0][value]'], [], $button);
    $this->assertSession()->pageTextNotContains($success);

    // Submit a 'ham' message.  This should be accepted.
    $this->assertHamSubmit($url, ['subject[0][value]', 'message[0][value]'], [], $button);
    $this->assertSession()->pageTextContains($success);

    // Submit an 'unsure' message. This should not be accepted either.
    $this->assertUnsureSubmit($url, ['subject[0][value]', 'message[0][value]'], [], $button, $success);
  }

}
