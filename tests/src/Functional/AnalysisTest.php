<?php

namespace Drupal\Tests\akismet\Functional;

/**
 * Tests text analysis functionality.
 *
 * @group akismet
 */
class AnalysisTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'akismet_test',
  ];

  /**
   * Disable the default set up.
   *
   * @var bool
   */
  public $disableDefaultSetup = TRUE;

  /**
   * Use local mock server.
   *
   * @var bool
   */
  protected $useLocal = TRUE;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // The test form doesn't have a moderation callback, so we should not leave
    // the default value of 'moderate' for unsure. Ideally this would be handled
    // within the config entity.
    $this->setProtection('akismet_test_post_form', ['unsure' => 'discard']);
  }

  /**
   * Tests basic unsure submission flow.
   */
  public function testUnsure() {
    $this->drupalGet('akismet-test/form');
    $edit = [
      'title' => 'unsure',
    ];
    $this->submitForm($edit, 'Save');

    $edit = [
      'title' => 'unsure unsure',
    ];
    $this->submitForm($edit, 'Save');
  }

  /**
   * Tests unsure post with other validation errors.
   */
  public function testUnsureValidation() {
    // The 'title' field is required. Omit its value to verify that validation
    // failures do not interfere with the spam filter.
    $this->drupalGet('akismet-test/form');
    $edit = [
      'body' => 'unsure',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);
    $this->assertSession()->pageTextContains('Title field is required.');
    $edit = [
      'body' => 'unsure unsure',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);
    $this->assertSession()->pageTextContains('Title field is required.');
    $edit = [
      'body' => 'unsure unsure unsure',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);
    $this->assertSession()->pageTextContains('Title field is required.');
    $edit = [
      'title' => 'unsure',
      'body' => 'unsure',
    ];
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextNotContains('Title field is required.');
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);
  }

  /**
   * Tests ham posts.
   */
  public function testHam() {
    $this->assertHamSubmit('akismet-test/form', ['title', 'body'], [], 'Save', 'Successful form submission.');
  }

  /**
   * Tests spam posts.
   */
  public function testSpam() {
    $this->assertSpamSubmit('akismet-test/form', ['title', 'body'], [], 'Save', 'Successful form submission.');
  }

}

