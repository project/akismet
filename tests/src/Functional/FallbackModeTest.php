<?php

namespace Drupal\Tests\akismet\Functional;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\akismet\Form\Settings;

/**
 * Tests expected fallback behavior when Akismet servers are not available.
 *
 * @group akismet
 */
class FallbackModeTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
  ];

  /**
   * Do not create keys in the base setup.
   *
   * @var bool
   */
  protected $createKeys = FALSE;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);
  }

  /**
   * Tests that form submissions are blocked when servers are unreachable.
   */
  public function testBlock() {
    $config = \Drupal::configFactory()->getEditable('akismet.settings');
    $this->setProtection('user_pass');
    $this->setProtection('user_register_form');

    // Set the fallback strategy to 'blocking mode'.
    $config->set('fallback', Settings::AKISMET_FALLBACK_BLOCK)->save();

    // Make all requests to Akismet fail.
    \Drupal::state()->set('akismet.testing_use_local_invalid', TRUE);

    // Check the password form.
    $this->drupalGet('user/password');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextContains(self::FALLBACK_MESSAGE);

    // Verify that the form cannot be submitted.
    $edit = [
      'name' => $this->adminUser->getAccountName(),
    ];
    $this->submitForm($edit, 'Submit');
    $this->assertSession()->pageTextNotContains('Further instructions have been sent to your email address.');

    // Check the register form.
    $this->drupalGet('user/register');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextContains(self::FALLBACK_MESSAGE);

    // Verify that the form cannot be submitted.
    $edit = [
      'mail' => $this->randomString(),
    ];
    $this->submitForm($edit, 'Create new account');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextContains(self::FALLBACK_MESSAGE);
  }

  /**
   * Tests that form submissions are accepted when servers are unreachable.
   */
  public function testAccept() {
    $config = \Drupal::configFactory()->getEditable('akismet.settings');
    $this->setProtection('user_pass');
    $this->setProtection('user_register_form');

    // Set the fallback strategy to 'accept mode'.
    $config->set('fallback', Settings::AKISMET_FALLBACK_ACCEPT)->save();

    // Make all requests to Akismet fail.
    \Drupal::state()->set('akismet.testing_use_local_invalid', TRUE);

    // Check the password form.
    $this->drupalGet('user/password');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextNotContains(self::FALLBACK_MESSAGE);

    // Verify that the form can be submitted.
    $edit = [
      'name' => $this->adminUser->getAccountName(),
    ];
    $this->submitForm($edit, 'Submit');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertPasswordInstructionMessage();
    $this->assertSession()->pageTextNotContains(self::FALLBACK_MESSAGE);

    // Check the registration form.
    $this->drupalGet('user/register');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextNotContains(self::FALLBACK_MESSAGE);

    // Verify that the form can be submitted.
    $edit = [
      'mail' => 'testme@test.com',
      'name' => $this->randomString(),
    ];
    $this->submitForm($edit, 'Create new account');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextNotContains(self::FALLBACK_MESSAGE);
  }

  /**
   * Tests partially failing communication is handled correctly.
   *
   * Form submissions should be accepted when only last request attempt to
   * Akismet servers succeeds.
   */
  public function testFailover() {
    $config = \Drupal::configFactory()->getEditable('akismet.settings');
    $this->setProtection('user_pass');

    // Set the fallback strategy to 'blocking mode', so that if the failover
    // mechanism does not work, we would expect to get a warning.
    $config->set('fallback', Settings::AKISMET_FALLBACK_BLOCK)->save();

    // Make all requests to Akismet fail.
    \Drupal::state()->set('akismet.testing_use_local_invalid', TRUE);
    // Enable pseudo server fail-over.
    // @see AkismetDrupalTestInvalid::handleRequest()
    \Drupal::state()->set('akismet_testing_server_failover', TRUE);

    // Validate that a user is not blocked from submitting the request password
    // form.
    $this->drupalGet('user/password');
    $this->assertAkismetWatchdogMessages(RfcLogLevel::EMERGENCY);
    $this->assertSession()->pageTextNotContains(self::FALLBACK_MESSAGE);
    // The watchdog errors don't really help us here due to the error
    // nature of the failover scenario.
    $this->assertWatchdogErrors = FALSE;
    $this->drupalGet('user/password');
    $this->submitForm(['name' => $this->adminUser->getAccountName()], 'Submit');
    $this->assertPasswordInstructionMessage();
  }

}
