<?php

namespace Drupal\Tests\akismet\Functional;

/**
 * Tests that a configured endpoint behaves the same as specified endpoints.
 *
 * @group akismet
 */
class ResponseConfiguredTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
  ];

  /**
   * Test that a configured endpoint behaves the same as specified endpoints.
   */
  public function testConfiguredUrl() {
    // Set the configured URL to be the same as the test server endpoint.
    $this->markTestSkipped('Setting the API URL is currently not supported. See https://www.drupal.org/project/akismet/issues/3209449');
    $test_url = $this->getAbsoluteURL('akismet-test');
    // Strip the protocol from the URL.
    $stripped = preg_replace('/^[a-z]+:\\/\\//i', '', $test_url);
    \Drupal::configFactory()->getEditable('akismet.settings')->set('api_endpoint', $stripped)->save();

    // Tell the test to not use the local test (so it will use the setting).
    \Drupal::state()->set('akismet.testing_use_local', FALSE);

    // Clear out Akismet local variable so that it can be instantiated with the
    // correct endpoint.
    $akismet = $this->getClient(TRUE);

    // The DrupalTest class should now use the local test server.
    $result = $akismet->verifyKey();
    $this->assertTrue($result === TRUE, 'Key should be verifiable.');
  }

}
