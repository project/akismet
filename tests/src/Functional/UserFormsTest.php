<?php

namespace Drupal\Tests\akismet\Functional;

use Composer\Semver\Comparator;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests protection of User module forms.
 *
 * @group akismet
 */
class UserFormsTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'akismet_test',
  ];

  /**
   * User to run tests with.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $webUser;

  /**
   * Make sure that the request password form is protected correctly.
   */
  public function testUserPassword() {
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('user_pass');
    $this->drupalLogout();

    // Create a new user.
    $this->webUser = $this->drupalCreateUser([]);

    $this->drupalGet('user/password');

    // Try to reset the user's password by specifying an spammy name.
    $edit = ['name' => 'spam'];
    $this->drupalGet('user/password');
    $this->submitForm($edit, 'Submit');
    $this->assertSession()->pageTextContains('Your submission has triggered the spam filter and will not be accepted.');

    // Try to reset the user's password by specifying a valid name.
    $edit = ['name' => $this->webUser->getAccountName()];
    $this->drupalGet('user/password');
    $this->submitForm($edit, 'Submit');
    $this->assertPasswordInstructionMessage();

    $this->assertSession()->pageTextNotContains('Your submission has triggered the spam filter and will not be accepted.');
  }

  /**
   * Make sure that the user registration form is protected correctly.
   */
  public function testUserRegister() {
    $user_storage = \Drupal::entityTypeManager()->getStorage('user');

    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('user_register_form');
    $this->drupalLogout();

    // Retrieve initial count of registered users.
    $users = $user_storage->loadMultiple();
    $count_initial = count($users);

    // Try to register with a spammy username. Make sure the user did not
    // successfully register.
    $name = $this->randomMachineName();
    $edit = [
      'name' => 'spam',
      'mail' => 'spam@example.com',
    ];
    $this->drupalGet('user/register');
    $this->submitForm($edit, 'Create new account');
    $this->assertFalse(user_load_by_name($name), 'The user who attempted to register cannot be found in the database when they used a spammy username.');

    // Verify that user count is still the same.
    $users = $user_storage->loadMultiple();
    $this->assertEquals($count_initial, count($users), 'No new user record has been created.');

    // Try to register with a valid username. Make sure the user was able
    // to successfully register.
    $name = $this->randomMachineName();
    $edit = [
      'name' => $name,
      'mail' => $name . '@example.com',
    ];
    $this->drupalGet('user/register');

    $this->submitForm($edit, 'Create new account');
    $this->assertSession()->pageTextContains('A welcome message with further instructions has been sent to your email address.');
    /** @var \Drupal\user\Entity\User $account */
    $account = user_load_by_name($edit['name']);
    $this->assertIsObject($account, 'New user found after using a valid name.');
    $data = $this->assertAkismetData('user', $account->id());
    $this->assertEqualWithMessage($data->moderate, 0, '$data->moderate');

    // Verify that the user account is deleted after reporting it as spam.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('user/' . $account->id() . '/cancel');
    $edit = [
      'user_cancel_method' => 'user_cancel_delete',
      'akismet[feedback]' => 'spam',
    ];
    $this->submitForm($edit, 'edit-submit');
    $user_storage->resetCache();
    $account = $user_storage->load($account->id());
    $this->assertNull($account, 'Reported user account not found.');
  }

  /**
   * Tests unsure setting for user registration.
   */
  public function testUserRegisterUnsureModerate() {
    $user_storage = \Drupal::entityTypeManager()->getStorage('user');

    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('user_register_form', [], [
      'unsure' => 'moderate',
    ]);
    $this->drupalLogout();

    // Retrieve initial count of registered users.
    $count_initial = count($user_storage->loadMultiple());

    // Verify that a spam user registration is still blocked.
    $this->drupalGet('user/register');
    $edit = [
      'name' => 'spam',
      'mail' => 'spam@example.com',
    ];
    $this->submitForm($edit, 'Create new account');
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);
    $count_new = count($user_storage->loadMultiple());
    $this->assertEquals($count_initial, $count_new, 'Existing user count found.');
    $this->assertFalse(user_load_by_name($edit['name']), 'New user not found.');

    // Verify that an unsure user is moderated.
    $this->drupalGet('user/register');
    $edit = [
      'name' => 'unsure',
      'mail' => 'unsure@example.com',
    ];
    $this->submitForm($edit, 'Create new account');

    $account = user_load_by_name($edit['name']);
    $this->assertIsObject($account, 'New user found after registering with unsure name.');
    $this->assertFalse($account->isActive(), 'New user account is pending approval.');
    $data = $this->assertAkismetData('user', $account->id());
    $this->assertEqualWithMessage($data->moderate, 1, '$data->moderate');

    // Verify that the user account is deleted after reporting it as spam.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('user/' . $account->id() . '/cancel');
    $edit = [
      'user_cancel_method' => 'user_cancel_delete',
      'akismet[feedback]' => 'spam',
    ];
    $this->submitForm($edit, 'edit-submit');
    $user_storage->resetCache();
    $account = $user_storage->load($account->id());
    $this->assertNull($account, 'Reported user account not found.');
  }

  /**
   * Tests text analysis protection for user registration form.
   */
  public function testUserRegisterSpamModerate() {
    $user_storage = \Drupal::entityTypeManager()->getStorage('user');

    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('user_register_form', [], [
      'unsure' => 'moderate',
      'discard' => 0,
    ]);
    $this->drupalLogout();

    foreach (['spam', 'unsure'] as $type) {
      // Verify that a spam user registration is moderated.
      $this->drupalGet('user/register');
      $edit = [
        'name' => $type,
        'mail' => $type . '@example.com',
      ];
      $this->submitForm($edit, 'Create new account');

      $account = user_load_by_name($type);
      $this->assertIsObject($account, "New user found after registering with $type name.");
      $this->assertFalse($account->isActive(), "New $type user account is pending approval.");
      $data = $this->assertAkismetData('user', $account->id());
      $this->assertEqualWithMessage($data->moderate, 1, '$data->moderate');
      // Verify that the user account is deleted after reporting it as spam.
      $this->drupalLogin($this->adminUser);
      $this->drupalGet('user/' . $account->id() . '/cancel');
      $edit = [
        'user_cancel_method' => 'user_cancel_delete',
        'akismet[feedback]' => 'spam',
      ];
      $this->submitForm($edit, 'edit-submit');
      $user_storage->resetCache();
      $account = $user_storage->load($account->id());
      $this->assertNull($account, 'Reported user account not found.');
      $this->drupalLogout();
    }
  }

  /**
   * Test that we can register a user.
   */
  public function testUserRegisterFields() {
    $this->addPermissionsToAdmin([
      'administer user fields',
      'administer user form display',
      'administer user display',
      'administer users',
      'administer account settings',
    ]);
    $this->drupalLogin($this->adminUser);
    // Add additional fields to the user profile.
    $types = [
      'text',
      'string_long',
      'text_with_summary',
      'text_long',
      'string',
      'email',
    ];
    $fields = [];
    foreach ($types as $type) {
      $field_name = $type;
      $fields[$type] = $field_name;
      FieldStorageConfig::create([
        'field_name' => $field_name,
        'entity_type' => 'user',
        'type' => $type,
      ])->save();

      FieldConfig::create([
        'field_name' => $field_name,
        'entity_type' => 'user',
        'label' => $field_name,
        'bundle' => 'user',
        'required' => FALSE,
      ])->save();

      \Drupal::service('entity_display.repository')->getFormDisplay('user', 'user')
        ->setComponent($field_name)
        ->save();
    }
    \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();

    // Enable text analysis protection for user registration form.
    $this->setProtectionUi(
      'user_register_form',
      $fields,
      ['unsure' => 'moderate']
    );
    $this->drupalLogout();

    // Test each supported field separately.
    foreach ($fields as $type => $field_name) {
      $name = $this->randomMachineName();
      $form_field_name = $field_name . '[0][value]';
      $this->drupalGet('user/register');

      $edit = [
        'name' => $name,
        'mail' => $field_name . '@example.com',
        $form_field_name => $type === 'email' ? 'unsure@example.com' : 'unsure',
      ];
      $this->submitForm($edit, 'Create new account');
      $this->assertSession()->pageTextContains('Your account is currently pending approval by the site administrator.');
      $this->assertIsObject(user_load_by_name($name), 'New user was found in database.');
    }
  }

}
