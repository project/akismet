<?php

namespace Drupal\Tests\akismet\Functional;

/**
 * Tests that users having higher privileges can bypass Akismet protection.
 *
 * @group akismet
 */
class BypassAccessTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'akismet_test',
  ];

  /**
   * Tests 'bypass access' property of registered forms.
   */
  public function testBypassAccess() {
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('akismet_test_post_form');
    $this->drupalLogout();

    // Create a regular user and submit form.
    $web_user = $this->drupalCreateUser([]);
    $this->drupalLogin($web_user);
    $edit = [
      'title' => $this->randomString(),
      'body' => 'ham',
    ];
    $this->drupalGet('akismet-test/form');
    $this->submitForm($edit, 'Save');
    $this->assertTestSubmitData();

    // Ensure a user having one of the permissions to bypass access can post
    // spam without triggering the spam protection.
    $this->drupalLogin($this->adminUser);

    $edit = [
      'title' => $this->randomString(),
      'body' => 'spam',
    ];
    $this->drupalGet('akismet-test/form');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextNotContains(self::SPAM_MESSAGE);
    $this->assertTestSubmitData();

    // Log in back the regular user and try to submit spam.
    $this->drupalLogin($web_user);
    $this->drupalGet('akismet-test/form');

    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);
  }

}
