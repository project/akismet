<?php

namespace Drupal\Tests\akismet\Functional;

use Composer\Semver\Comparator;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Html;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Session\AccountInterface;
use Drupal\akismet\Controller\FormController;
use Drupal\akismet\Entity\Form;
use Drupal\akismet\Entity\FormInterface;
use Drupal\akismet\Storage\ResponseDataStorage;
use Drupal\akismet\Utility\Logger;
use Drupal\Tests\BrowserTestBase;

/**
 * Class AkismetTestBase.
 *
 * Common functionality for our test classes.
 *
 * @package Drupal\Tests\akismet\Functional
 */
abstract class AkismetTestBase extends BrowserTestBase {

  use AkismetCommentTestTrait;

  /**
   * Set default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Fallback message.
   *
   * The text the user should see when they are blocked from submitting a form
   * because the Akismet servers are unreachable.
   */
  const FALLBACK_MESSAGE = 'The spam filter installed on this site is currently unavailable. Per site policy, we are unable to accept new submissions until that problem is resolved. Please try resubmitting the form in a couple of minutes.';

  /**
   * The text the user should see if there submission was determined to be spam.
   */
  const SPAM_MESSAGE = 'Your submission has triggered the spam filter and will not be accepted.';

  /**
   * Indicates if the default setup permissions and keys should be skipped.
   *
   * @var bool
   */
  public $disableDefaultSetup = FALSE;

  /**
   * A user with permissions to administer Akismet.
   *
   * @var \Drupal\user\Entity\User
   */
  public $adminUser;

  /**
   * Tracks Akismet messages across tests.
   *
   * @var array
   */
  protected $messages = [];

  /**
   * The public key used during testing.
   *
   * @var string
   */
  protected $publicKey;

  /**
   * The private key used during testing.
   *
   * @var string
   */
  protected $privateKey;

  /**
   * Flag indicating whether to automatically create testing API keys.
   *
   * If testing_mode is enabled, Akismet module automatically uses the
   * AkismetDrupalTest client implementation. This implementation automatically
   * creates testing API keys when being instantiated (and ensures to re-create
   * testing API keys in case they vanish). The behavior is executed by default,
   * but depends on the 'akismet.testing_create_keys' state variable being TRUE.
   *
   * Some functional test cases verify that expected errors are displayed in
   * case no or invalid API keys are configured. For such test cases, set this
   * flag to FALSE to skip the automatic creation of testing keys.
   *
   * @var bool
   *
   * @see AkismetDrupalTest::$createKeys
   * @see AkismetDrupalTest::createKeys()
   */
  protected $createKeys = TRUE;

  /**
   * Whether local test server should be used in place of the real Akismet API.
   *
   * @var bool
   *
   * @todo Remove this? Real akismet always needs an API key, so it is not
   *   really practical for testing. Set to true for now.
   */
  protected $useLocal = TRUE;

  /**
   * Tracks Akismet session response IDs.
   *
   * @var array
   */
  protected $responseIds = [];

  /**
   * Akismet configuration settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $akismetSettings;

  /**
   * An instance of the Drupal Akismet client.
   *
   * @var \Drupal\akismet\Client\DrupalClientInterface
   */
  protected $akismet;

  /**
   * The current log level to use on the site after testing.
   *
   * @var int
   * @todo Verify this is still needed (and an int).
   */
  protected $originalLogLevel;

  /**
   * Used to turn on/off watchdog assertions during a portion of a test.
   *
   * @var bool
   */
  protected $assertWatchdogErrors = TRUE;

  /**
   * Indicates if default Akismet testing setup should be created.
   *
   * @var bool
   */
  protected $setupAkismet = TRUE;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    $this->messages = [];

    parent::setUp();

    if (!$this->setupAkismet) {
      return;
    }

    $state = [];

    // Set the spun up instance to use the local test class.
    if ($this->useLocal) {
      $state['akismet.testing_use_local'] = TRUE;
    }

    // Omit warnings and create keys only when the test asked for it.
    $state += [
      'akismet.omit_warning' => TRUE,
    ];
    \Drupal::state()->setMultiple($state);

    // Set log level.
    $settings = \Drupal::configFactory()->getEditable('akismet.settings');
    $settings->set('log_level', RfcLogLevel::DEBUG);
    // Set test mode.
    $settings->set('test_mode.enabled', TRUE);
    $settings->set('api_key', 'validkey');
    $settings->save();

    if ($this->disableDefaultSetup) {
      return;
    }

    $permissions = [
      'access administration pages',
      'administer akismet',
      'administer nodes',
      'access content overview',
      'administer content types',
      'administer permissions',
      'administer users',
      'bypass node access',
      'access comments',
      'post comments',
      'skip comment approval',
      'administer comments',
      'administer account settings',
    ];
    $this->adminUser = $this->drupalCreateUser($permissions);
  }

  /**
   * {@inheritDoc}
   */
  protected function tearDown(): void {
    if ($this->assertWatchdogErrors) {
      $this->assertAkismetWatchdogMessages();
    }
    $this->akismet = NULL;

    parent::tearDown();
  }

  /**
   * Assert any watchdog messages based on their severity.
   *
   * This function can be (repeatedly) invoked to assert new watchdog messages.
   * All watchdog messages with a higher severity than RfcLogLevel::NOTICE are
   * considered as "severe".
   *
   * @param int $max_severity
   *   (optional) A maximum watchdog severity level message constant that log
   *   messages must have to pass the assertion. All messages with a higher
   *   severity will fail. Defaults to RfcLogLevel::NOTICE. If a severity level
   *   higher than RfcLogLevel::NOTICE is passed, then at least one severe
   *   message is expected.
   */
  protected function assertAkismetWatchdogMessages(int $max_severity = RfcLogLevel::NOTICE) {
    // Ensure that all messages have been written before attempting to verify
    // them. Actions executed within the test class may lead to log messages,
    // but those get only logged when hook_exit() is triggered.
    // akismet.module may not be installed by a test and thus not loaded yet.
    Logger::writeLog();
    $database = \Drupal::database();

    \Drupal::moduleHandler()->loadInclude('dblog', 'inc', 'dblog.admin');

    $this->messages = [];
    $query = $database->select('watchdog', 'w')
      ->fields('w')
      ->orderBy('w.timestamp', 'ASC');

    // The comparison logic applied in this function is a bit confusing, since
    // the values of watchdog severity level constants defined by RFC 3164 are
    // negated to their actual "severity level" meaning:
    // RfcLogLevel::EMERGENCY is 0, RfcLogLevel::NOTICE is 5, RfcLogLevel::DEBUG
    // is 7.
    $fail_expected = ($max_severity < RfcLogLevel::NOTICE);
    $had_severe_message = FALSE;
    foreach ($query->execute() as $row) {
      $this->messages[$row->wid] = $row;
      // Only messages with a maximum severity of $max_severity or less severe
      // messages must pass. More severe messages need to fail. See note about
      // severity level constant values above.
      $output = $this->formatMessage($row);
      if ($row->severity >= $max_severity) {
        // Visually separate debug log messages from other messages.
        if ($row->severity == RfcLogLevel::DEBUG) {
          $this->assertTrue(TRUE, $output);
        }
        else {
          $this->assertTrue(TRUE, Html::escape($row->type) . ' (' . $row->severity . '): ' . $output);
        }
      }
      else {
        $this->fail(Html::escape($row->type) . ' (' . $row->severity . '): ' . $output);
      }
      // In case a severe message is expected, non-severe messages always pass,
      // since we would trigger a false positive test failure otherwise.
      // However, in order to actually assert the expectation, there must have
      // been at least one severe log message.
      $had_severe_message = ($had_severe_message || $row->severity < RfcLogLevel::NOTICE);
    }
    // Assert that there was a severe message, in case we expected one.
    if ($fail_expected && !$had_severe_message) {
      $this->fail('Severe log message should be found.');
    }
    // Delete processed watchdog messages.
    if (!empty($this->messages)) {
      $seen_ids = array_keys($this->messages);

      $database->delete('watchdog')->condition('wid', $seen_ids, 'IN')->execute();
    }
  }

  /**
   * Retrieve sent request parameter values from testing server implementation.
   *
   * @param string $resource
   *   (optional) The resource name to retrieve submitted values from. Defaults
   *   to 'content'.
   * @param bool $retain
   *   (optional) Whether to retain the (last) record being read. Defaults to
   *   FALSE; i.e., the record being read is removed.
   *
   * @see AkismetTestBase::resetServerRecords()
   */
  protected function getServerRecord(string $resource = 'content', bool $retain = FALSE) {
    $function = 'akismet_test_server_' . $resource;

    // Retrieve last recorded values.
    $state = \Drupal::state();
    $storage = $state->get($function, []);
    $return = ($retain ? end($storage) : array_shift($storage));
    $state->set($function, $storage);

    return $return;
  }

  /**
   * Resets recorded server values.
   *
   * @param string $resource
   *   (optional) The resource name to reset records of. Defaults to 'content'.
   *
   * @see AkismetTestBase::getServerRecord()
   */
  protected function resetServerRecords(string $resource = 'content') {
    $function = 'akismet_test_server_' . $resource;

    // Delete the variable.
    \Drupal::state()->delete($function);
  }

  /**
   * Instantiate a Akismet client and make it available on $this->akismet.
   */
  protected function getClient($force = FALSE) {
    if ($force || !isset($this->akismet)) {
      if ($force) {
        $this->rebuildContainer();
      }
      $this->akismet = \Drupal::service('akismet.client');
    }
    return $this->akismet;
  }

  /**
   * Formats a database log message.
   *
   * This is copied from DbLogController and should be called from there
   * instead.
   *
   * @param object $row
   *   The record from the watchdog table. The object properties are: wid, uid,
   *   severity, type, timestamp, message, variables, link, name.
   *
   * @return string|false
   *   The formatted log message or FALSE if the message or variables properties
   *   are not set.
   */
  public function formatMessage($row) {
    // Check for required properties.
    if (isset($row->message) && isset($row->variables)) {
      // Messages without variables or user specified text.
      if ($row->variables === 'N;') {
        $message = $row->message;
      }
      // Message to translate with injected variables.
      else {
        $message = new FormattableMarkup($row->message, unserialize($row->variables));
      }
    }
    else {
      $message = FALSE;
    }
    return $message;
  }

  /**
   * Saves a akismet_form entity to protect a given form with Akismet.
   *
   * @param string $form_id
   *   The form id to protect.
   * @param array $values
   *   (optional) An associative array of properties to additionally set on the
   *   akismet_form entity.
   *
   * @return int
   *   The save status, as returned by akismet_form_save().
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setProtection(string $form_id, array $values = []): int {
    /** @var \Drupal\akismet\Entity\FormInterface $akismet_form */
    if (!$akismet_form = \Drupal::entityTypeManager()->getStorage('akismet_form')->load($form_id)) {
      $akismet_form = Form::create();
      $akismet_form->initialize($form_id);
    }
    if ($values) {
      foreach ($values as $property => $value) {
        $akismet_form->$property = $value;
      }
    }

    return $akismet_form->save();
  }

  /**
   * Assert that Akismet session data was stored for a submission.
   *
   * @param string $entity
   *   The entity type to search for in {akismet}.
   * @param mixed $id
   *   The entity id to search for in {akismet}.
   * @param string $response_type
   *   (optional) The type of ID to assert; e.g., 'contentId'.
   * @param mixed $response_id
   *   (optional) The ID of $type to assert additionally.
   */
  protected function assertAkismetData(string $entity, $id, string $response_type = '', $response_id = NULL) {
    $data = ResponseDataStorage::loadByEntity($entity, $id);
    $this->assertIsObject($data, "Akismet session data for " . $entity . $id . "exists: <pre>" . var_export($data, TRUE) . "</pre>");
    if (isset($response_id)) {
      $this->assertEqualWithMessage($data->{$response_type}, $response_id,
        "Stored " . $response_type . "ID"
      );
    }

    return $data;
  }

  /**
   * Assert that no Akismet session data exists for a certain entity.
   */
  protected function assertNoAkismetData($entity, $id) {
    $data = ResponseDataStorage::loadByEntity($entity, $id);
    $this->assertFalse(
      $data,
      'No Akismet session data exists for <em class="placeholder">' . $entity . '</em> ' . $id . "."
    );
  }

  /**
   * Test submitting a form with 'spam' values.
   *
   * @param \Drupal\Core\Url|string $url
   *   The URL of the form, or NULL to use the current page.
   * @param array $spam_fields
   *   An array of form field names to inject spam content into.
   * @param array $edit
   *   An array of non-spam form values used in drupalPost().
   * @param string $button
   *   The text of the form button to click in drupalPost().
   * @param string $success_message
   *   An optional message to test does not appear after submission.
   *
   * @todo Assess whether default parameter valius are appropriate.
   */
  protected function assertSpamSubmit($url, array $spam_fields, array $edit = [], string $button = '', string $success_message = '') {
    $edit += array_fill_keys($spam_fields, 'spam');

    if (!empty($url)) {
      $this->drupalGet($url);
    }

    $this->submitForm($edit, $button);
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);
    if ($success_message) {
      $this->assertSession()->pageTextNotContains($success_message);
    }
  }

  /**
   * Test submitting a form with 'ham' values.
   *
   * @param \Drupal\Core\Url|string $url
   *   The URL of the form, or NULL to use the current page.
   * @param array $ham_fields
   *   An array of form field names to inject ham content into.
   * @param array $edit
   *   An array of non-spam form values used in drupalPost().
   * @param string $button
   *   The text of the form button to click in drupalPost().
   * @param string $success_message
   *   An optional message to test does appear after submission.
   */
  protected function assertHamSubmit($url, array $ham_fields, array $edit = [], string $button = '', string $success_message = '') {
    $edit += array_fill_keys($ham_fields, 'ham');

    if (!empty($url)) {
      $this->drupalGet($url);
    }

    $this->submitForm($edit, $button);
    $this->assertSession()->pageTextNotContains(self::SPAM_MESSAGE);
    if ($success_message) {
      $this->assertSession()->pageTextContains($success_message);
    }
  }

  /**
   * Test submitting a form with unsure values.
   *
   * @param \Drupal\Core\Url|string $url
   *   The URL of the form, or NULL to use the current page.
   * @param array $unsure_fields
   *   An array of form field names to inject unsure content into.
   * @param array $edit
   *   An array of non-spam form values used in drupalPost().
   * @param string $button
   *   The text of the form button to click in drupalPost().
   * @param string $success_message
   *   An optional message to test does appear after sucessful form and CAPTCHA
   *   submission.
   */
  protected function assertUnsureSubmit($url, array $unsure_fields, array $edit = [], string $button = '', string $success_message = '') {
    $edit += array_fill_keys($unsure_fields, 'unsure');
    $this->drupalGet($url);
    $this->submitForm($edit, $button);
    $this->assertSession()->pageTextContains(self::SPAM_MESSAGE);
    if ($success_message) {
      $this->assertSession()->pageTextNotContains($success_message);
    }
  }

  /**
   * Asserts a successful akismet_test_form submission.
   *
   * @param int $old_mid
   *   (optional) The existing test record id to assert.
   *
   * @return string
   *   The actual mid.
   */
  protected function assertTestSubmitData(int $old_mid = NULL): string {
    $this->assertSession()->pageTextContains('Successful form submission.');
    $mid = $this->getFieldValueByName('mid');
    if (isset($old_mid)) {
      $this->assertEqualWithMessage($mid, $old_mid, 'Test record id');
    }
    else {
      $this->assertTrue($mid > 0, 'Test record id ' . $mid . ' found.');
    }
    return $mid;
  }

  /**
   * Calls assertEqual() with a message template.
   *
   * Checks to see whether two values, which belong to the same variable name or
   * identifier, are equal and logs a readable assertion message.
   *
   * @param mixed $first
   *   The first value to check.
   * @param mixed $second
   *   The second value to check.
   * @param string $name
   *   A name or identifier to use in the assertion message.
   *
   * @see AkismetTestBase::assertNotEqualWithMessage()
   */
  protected function assertEqualWithMessage($first, $second, $name) {
    // $message = t("@name: @first is equal to @second.", [
    //   '@name' => $name,
    //   '@first' => var_export($first, TRUE),
    //   '@second' => var_export($second, TRUE),
    // ]);
    $message = $name . ": " . var_export($first, TRUE) . " is equal to " . var_export($second, TRUE) . ".";
    $this->assertEquals($first, $second, $message);
  }

  /**
   * Calls assertNotEqual() with a message template.
   *
   * Checks to see whether two values, which belong to the same variable name or
   * identifier, are not equal and logs a readable assertion message.
   *
   * @param mixed $first
   *   The first value to check.
   * @param mixed $second
   *   The second value to check.
   * @param string $name
   *   A name or identifier to use in the assertion message.
   *
   * @see AkismetTestBase::assertEqualWithMessage()
   */
  protected function assertNotEqualWithMessage($first, $second, $name) {
    // $message = t("@name: '@first' is not equal to '@second'.", [
    //   '@name' => $name,
    //   '@first' => var_export($first, TRUE),
    //   '@second' => var_export($second, TRUE),
    // ]);
    $message = $name . ": '" . var_export($first, TRUE) . "' is not equal to '" . var_export($second, TRUE) . "'.";
    $this->assertNotEquals($first, $second, $message);
  }

  /**
   * Retrieve a field value by ID.
   */
  protected function getFieldValueById($id): string {
    $page = $this->getSession()->getPage();
    // Not sure why a regular findField will not work here.
    $field = $page->find('xpath', '//input[@id="' . $id . '"]');
    return (string) $field->getValue();
  }

  /**
   * Retrieve a field value by name.
   */
  protected function getFieldValueByName($name): string {
    $page = $this->getSession()->getPage();
    // Not sure why a regular findField will not work here.
    $field = $page->find('xpath', '//input[@name="' . $name . '"]');
    return (string) $field->getValue();
  }

  /**
   * Configure Akismet protection for a given form.
   *
   * @param string $form_id
   *   The form id to configure.
   *   The Akismet protection mode for the form. (Currently unused, mode is
   *   always FormInterface::AKISMET_MODE_ANALYSIS and can currently not be
   *   changed).
   * @param array $fields
   *   (optional) A list of form elements to enable for text analysis. If
   *   omitted and the form registers individual elements, all fields are
   *   enabled by default.
   * @param array $edit
   *   (optional) An array of POST data to pass through to drupalPost() when
   *   configuring the form's protection.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function setProtectionUi(string $form_id, array $fields = NULL, array $edit = []) {
    // Always start from overview page, also to make debugging easier.
    $this->drupalGet('admin/config/content/akismet');

    // Determine whether the form is already protected.
    $exists = \Drupal::entityTypeManager()->getStorage('akismet_form')->load($form_id);
    // Add a new form.
    if (!$exists) {
      $this->drupalGet('admin/config/content/akismet/add-form', ['query' => ['form_id' => $form_id]]);
      $save = 'Create Protected Akismet Form';
    }
    // Edit an existing form.
    else {
      $this->assertSession()->linkByHrefExists('admin/config/content/akismet/form/' . $form_id . '/edit');
      $this->drupalGet('admin/config/content/akismet/form/' . $form_id . '/edit');
      $save = 'Update Protected Akismet Form';
    }

    // Set defaults.
    $edit += [
      'discard' => 1,
      'unsure' => 'discard',
    ];

    // Process the enabled fields.
    $form_list = FormController::getProtectableForms();
    $form_info = FormController::getProtectedFormDetails($form_id, $form_list[$form_id]['module']);

    foreach (array_keys($form_info['elements']) as $field) {
      if (!isset($fields) || in_array($field, $fields)) {
        // If the user specified all fields by default or to include this
        // field, set its checkbox value to TRUE.
        $edit['enabled_fields[' . rawurlencode($field) . ']'] = TRUE;
      }
      else {
        // Otherwise set the field's checkbox value to FALSE.
        $edit['enabled_fields[' . rawurlencode($field) . ']'] = FALSE;
      }
    }
    $this->submitForm($edit, $save);
    if (!$exists) {
      $this->assertSession()->pageTextContains('The form protection has been added.');
    }
    else {
      $this->assertSession()->pageTextContains('The form protection has been updated.');
    }
  }

  /**
   * Helper function to add permissions to the admin user.
   */
  protected function addPermissionsToAdmin(array $permissions) {
    $rid = current(array_diff(array_keys($this->adminUser->getRoles()), [AccountInterface::AUTHENTICATED_ROLE]));
    user_role_grant_permissions($rid, $permissions);
  }

  /**
   * Helper function to check for the appropriate password reset message.
   *
   * In Drupal 9.2.0, the meessage shown when requesting a new password has
   * been changed. When we no longer need to support Drupal 9.1 or lower,
   * this check can be removed.
   *
   * @see https://www.drupal.org/node/3189618
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  protected function assertPasswordInstructionMessage(): void {
    if (Comparator::greaterThanOrEqualTo(\Drupal::VERSION, '9.2.0')) {
      $this->assertSession()
        ->pageTextContains('an email will be sent with instructions to reset your password.');
    }
    else {
      $this->assertSession()
        ->pageTextContains('Further instructions have been sent to your email address.');
    }
  }

}
