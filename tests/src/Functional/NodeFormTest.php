<?php

namespace Drupal\Tests\akismet\Functional;

/**
 * Tests node form protection.
 *
 * @group akismet
 */
class NodeFormTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'akismet_test',
  ];

  /**
   * User to run tests with.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $webUser;

  /**
   * Reference to created node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);

    // @todo 'view own unpublished content' permission required to prevent a
    //   bogus access denied watchdog caused by a bug in Drupal core.
    // @see http://drupal.org/node/1429442
    $this->webUser = $this->drupalCreateUser([
      'create article content',
      'view own unpublished content',
    ]);
  }

  /**
   * Tests saving of Akismet data for protected node forms.
   *
   * Function node_form() uses a button-level form submit handler, which invokes
   * form-level submit handlers before a new node entity has been stored.
   * Therefore, the submitted form values do not contain a 'nid' yet, so Akismet
   * session data cannot be stored for the new node.
   */
  public function testData() {
    // Enable Akismet protection for Article nodes.
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('node_article_form');
    $this->drupalLogout();

    // Login and submit a node.
    $this->drupalLogin($this->webUser);
    $this->drupalGet('node/add/article');
    $edit = [
      'title[0][value]' => 'spam',
    ];
    $this->submitForm($edit, 'Save');
    $this->node = $this->drupalGetNodeByTitle($edit['title[0][value]']);
    $this->assertSession()->addressEquals('node/' . $this->node->id());
  }

  /**
   * Tests retaining of node form submissions.
   */
  public function testRetain() {
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi('node_article_form', NULL, [
      'discard' => 0,
    ]);
    $this->drupalLogout();

    // Login and submit a node.
    $this->drupalLogin($this->webUser);
    $this->drupalGet('node/add/article');
    $edit = [
      'title[0][value]' => 'spam',
      'body[0][value]' => 'spam',
    ];
    $this->submitForm($edit, 'Save');
    $this->node = $this->drupalGetNodeByTitle($edit['title[0][value]']);
    $this->assertFalse($this->node->isPublished(), 'Node containing spam was retained as unpublished.');
    $this->assertSession()->addressEquals('node/' . $this->node->id());
    $this->assertAkismetData('node', $this->node->id());
  }

}
