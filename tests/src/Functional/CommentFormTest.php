<?php

namespace Drupal\Tests\akismet\Functional;

use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Check that the comment submission form can be protected.
 *
 * @group akismet
 */
class CommentFormTest extends AkismetTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'dblog',
    'akismet',
    'node',
    'comment',
    'akismet_test_server',
    'akismet_test',
  ];

  const COMMENT_TEST_TYPE = 'test_type';

  /**
   * User to run tests with.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $webUser;

  /**
   * Node to run test on.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);

    $this->webUser = $this->drupalCreateUser([
      'create article content',
      'access comments',
      'post comments',
      'skip comment approval',
    ]);
    $this->node = $this->drupalCreateNode([
      'type' => 'article',
      'uid' => $this->webUser->uid,
    ]);

    $this->addCommentsToNode();
  }

  /**
   * Make sure that the comment submission form can be unprotected.
   */
  public function testUnprotectedCommentForm() {
    // Request the comment reply form.
    $this->drupalLogin($this->webUser);
    $this->drupalGet('node/' . $this->node->id());

    // Preview a comment that is 'spam'.
    $this->submitForm(['comment_body[0][value]' => 'spam'], 'Preview');

    // Save the comment and make sure it appears.
    $this->submitForm([], 'Save');

    // Assert that a comment that is known to be spam appears on the screen
    // after it is submitted.
    $this->assertSession()->responseContains('<p>spam</p>');
  }

  /**
   * Make sure that the comment submission form can be fully protected.
   */
  public function testTextAnalysisProtectedCommentForm() {
    // Test the basic comment form.
    $this->verifyCommentAnalysis('comment_comment_form', $this->node);
  }

  /**
   * Make sure that alternate comment type form can be fully protected.
   */
  public function testAlternateTextAnalysisProtectedCommentForm() {
    $page = $this->setupAlternateComments(self::COMMENT_TEST_TYPE, 'page');
    $this->verifyCommentAnalysis('comment_test_type_form', $page);
  }

  /**
   * Helper function to set up a new comment type and add it to a node.
   *
   * @param string $comment_type_id
   *   The id to use for the new comment type.
   * @param string $bundle
   *   A node bundle to create.
   *
   * @returns \Drupal\node\NodeInterface
   *   A new node of the specified bundle with the comment type field.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setupAlternateComments(string $comment_type_id, string $bundle): NodeInterface {
    $this->createCommentType($comment_type_id);
    $this->drupalCreateContentType(['type' => $bundle, 'name' => 'Page']);
    $this->addCommentsToNode($bundle, DRUPAL_OPTIONAL, $comment_type_id);

    return $this->drupalCreateNode([
      'type' => $bundle,
      'uid' => $this->webUser->id(),
    ]);
  }

  /**
   * Tests textual analysis on a comment form.
   *
   * @param string $form_id
   *   The form id of the comment form to protect.
   * @param \Drupal\node\NodeInterface $node
   *   A node that has the protected comment type as a form field.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function verifyCommentAnalysis(string $form_id, NodeInterface $node) {
    // Enable Akismet text-classification for comments.
    $this->drupalLogin($this->adminUser);
    $this->setProtectionUi($form_id);
    $this->drupalLogout();

    // Request the comment reply form.
    $this->drupalLogin($this->webUser);
    $this->drupalGet('node/' . $node->id());

    // Try to save a new 'spam' comment; it should be discarded.
    $this->drupalGet('node/' . $node->id());
    $original_number_of_comments = $this->getCommentCount($node->id());
    $this->assertSpamSubmit(NULL, ['comment_body[0][value]'], [], 'Save');
    $this->assertCommentCount($node->id(), $original_number_of_comments);

    // Try to save again; it should be discarded.
    $this->assertSpamSubmit(NULL, ['comment_body[0][value]'], [], 'Save');
    $this->assertCommentCount($node->id(), $original_number_of_comments);

    // Save a new 'ham' comment.
    $this->drupalGet('node/' . $node->id());
    $original_number_of_comments = $this->getCommentCount($node->id());
    $this->assertHamSubmit(NULL, ['comment_body[0][value]'], [], 'Save');
    // Assert aht A comment that is known to be ham appears on the screen after
    // it is submitted.
    $this->assertSession()->responseContains('<p>ham</p>');
    $this->assertCommentCount($node->id(), $original_number_of_comments + 1);
    $cids = $this->loadCommentsBySubject('ham');
    $this->assertAkismetData('comment', reset($cids));
  }

  /**
   * Return the number of comments for a node of the given node ID.
   *
   * We can't use comment_num_all() here, because that is statically cached and
   * therefore will not work correctly with the SimpleTest browser.
   */
  private function getCommentCount($nid) {
    // The field name for the comment field might be "comment" or "test_type"
    // depending on the test.
    $entity_field_manager = \Drupal::service('entity_field.manager');
    $node = Node::load($nid);
    if (array_key_exists(self::COMMENT_TEST_TYPE, $entity_field_manager->getFieldDefinitions($node->getEntityTypeId(), $node->bundle()))) {
      $field_name = self::COMMENT_TEST_TYPE;
    }
    else {
      $field_name = 'comment';
    }
    return \Drupal::database()->query(
      'SELECT comment_count FROM {comment_entity_statistics} WHERE entity_id = :nid and entity_type=:type and field_name=:field',
      [
        ':nid' => $nid,
        ':type' => 'node',
        ':field' => $field_name,
      ]
    )->fetchField();
  }

  /**
   * Test that the number of comments for a node matches an expected value.
   *
   * @param int $nid
   *   A node ID.
   * @param int $expected
   *   An integer with the expected number of comments for the node.
   * @param string $message
   *   An optional string with the message to be used in the assertion.
   */
  protected function assertCommentCount(int $nid, int $expected, string $message = '') {
    $actual = $this->getCommentCount($nid);
    if (!$message) {
      $message = 'Node ' . $nid . ' has ' . $actual . ' comment(s), expected ' . $expected . '.';
    }
    $this->assertEquals($actual, $expected, $message);
  }

}
