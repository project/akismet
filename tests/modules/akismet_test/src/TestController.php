<?php

namespace Drupal\akismet_test;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatch;
use Symfony\Component\HttpFoundation\Request;

/**
 * Contains basic page callbacks for the akismet test module.
 */
class TestController extends ControllerBase {

  /**
   * Route callback for resetting the view count.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The response.
   */
  public function resetViewCount(Request $request) {
    \Drupal::state()->delete('akismet_test.view_count');

    $route_match = RouteMatch::createFromRequest($request);
    return $this->redirect($route_match->getRouteName(), $route_match->getRawParameters()->all());
  }

}
