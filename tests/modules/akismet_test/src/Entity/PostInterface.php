<?php

namespace Drupal\akismet_test\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Define the interface for akismet test post submissions.
 *
 * @package Drupal\akismet_test\Entity
 */
interface PostInterface extends ContentEntityInterface {

  /**
   * Get the title.
   *
   * @return string
   *   The title of the post.
   */
  public function getTitle();

  /**
   * Set the title.
   *
   * @param string $title
   *   Title to set.
   */
  public function setTitle(string $title);

  /**
   * Get the body.
   *
   * @return string
   *   The body of the post.
   */
  public function getBody();

  /**
   * Set the body.
   *
   * @param string $body
   *   Body to set.
   */
  public function setBody(string $body);

  /**
   * Get the status.
   *
   * @return bool
   *   True for published, false for unpublished.
   */
  public function getStatus();

  /**
   * Set the status of the post.
   *
   * @param bool $status
   *   True for published, false for unpublished.
   */
  public function setStatus(bool $status);

  /**
   * Generate a storage record based on the entity data.
   *
   * @return array
   *   An array with keys representing the values in the entity.
   */
  public function getStorageRecord();

}
