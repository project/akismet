<?php

namespace Drupal\akismet_test\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the akismet_test post entity.
 */
class PostForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    $instance->messenger = $container->get('messenger');

    return $instance;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $state = \Drupal::state();

    $form = parent::buildForm($form, $form_state);

    // Due to #limit_validation_errors, submitting the form with the "Add"
    // button will only expose validated values in the submit handler, so our
    // storage may be incomplete. Therefore, the default values always have to
    // be overloaded.
    $stored_record = $entity->getStorageRecord();
    $stored_record += [
      'exclude' => '',
      'parent' => ['child' => ''],
      'field' => [],
    ];
    $storage = $form_state->get('akismet_test');
    $storage = empty($storage) ? [] : $storage;
    $stored_record = array_merge($stored_record, $storage);
    // Always add an empty field the user can submit.
    $stored_record['field']['new'] = '';
    $form_state->set('akismet_test', $stored_record);

    // Output a page view counter for page/form cache testing purposes.
    $count = $state->get('akismet_test.view_count', 0);

    $reset_link = Link::fromTextAndUrl(
      $this->t('Reset'),
      Url::fromRoute('akismet_test.views_reset', [], ['query' => $this->getDestinationArray()])
    );
    $form['views'] = [
      '#markup' => '<p>' . 'Views: ' . $count++ . ' ' . $reset_link->toString() . '</p>',
    ];
    $state->set('akismet_test.view_count', $count);

    $form['#tree'] = TRUE;
    $form['mid'] = [
      '#type' => 'hidden',
      '#value' => $stored_record['mid'],
    ];
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => 'Title',
      '#default_value' => $stored_record['title'],
      '#required' => TRUE,
    ];
    $form['body'] = [
      '#type' => 'textfield',
      '#title' => 'Body',
      '#default_value' => $stored_record['body'],
    ];
    $form['exclude'] = [
      '#type' => 'textfield',
      '#title' => 'Some other field',
      '#default_value' => $stored_record['exclude'],
    ];
    $form['parent']['child'] = [
      '#type' => 'textfield',
      '#title' => 'Nested element',
      '#default_value' => $stored_record['parent']['child'],
    ];
    $form['field'] = [
      '#type' => 'fieldset',
      '#title' => 'Field',
    ];

    $weight = 0;
    foreach ($stored_record['field'] as $delta => $value) {
      $form['field'][$delta] = [
        '#type' => 'textfield',
        '#title' => 'Field ' . $delta,
        '#default_value' => $value,
        '#weight' => $weight++,
      ];
    }
    $form['field']['new']['#weight'] = 999;
    $form['field']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Add',
      '#limit_validation_errors' => [['field']],
      '#submit' => ['::fieldSubmitForm'],
      '#weight' => 1000,
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => 'Published',
      '#default_value' => $stored_record['status'],
      // For simplicity, re-use Akismet module's administration permission.
      '#access' => \Drupal::currentUser()->hasPermission('administer akismet'),
    ];

    return $form;
  }

  /**
   * Form element submit handler for akismet_test_form().
   */
  public function fieldSubmitForm(array &$form, FormStateInterface $form_state) {
    // Remove all empty values of the multiple value field.
    $form_state->setValue('field', array_filter($form_state->getValue('field')));

    // Update the storage with submitted values.
    $storage_record = $form_state->getValues();

    // Store the new value and clear out the 'new' field.
    $new_field = $form_state->getValue(['field', 'new'], '');
    if (!empty($new_field)) {
      $storage_record['field'][] = $form_state->getValue(['field', 'new']);
      $form_state->setValue(['field', 'new'], '');
      $storage_record['field']['new'] = '';
      unset($storage_record['field']['add']);
      $input = $form_state->getUserInput();
      $input['field']['new'] = '';
      $form_state->setUserInput($input);
    }
    $form_state->set('akismet_test', $storage_record);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Form submit handler for akismet_test_form().
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Conditionally enable form caching.
    if (\Drupal::state()->get('akismet_test.cache_form', FALSE)) {
      $form_state->setCached(TRUE);
    }

    $new_field = $form_state->getValue(['field', 'new'], '');
    if (!empty($new_field)) {
      $field = $form_state->getValue('field');
      $field[] = $new_field;
      $form_state->setValue('field', $field);
    }
    parent::submitForm($form, $form_state);

    $this->messenger->addStatus('Successful form submission.');
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);
    // Redirect to stored entry.
    $form_state->setRedirect('entity.akismet_test_post.edit_form', ['akismet_test_post' => $this->entity->id()]);
  }

}
