<?php

namespace Drupal\akismet_test_server;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Default controller for the akismet test server.
 */
class ServerController extends ControllerBase {

  /**
   * Key to use for addressing state storage for storing request data.
   */
  const KEY_CONTENT = 'akismet_test_server_content';

  /**
   * Key to use for addressing state storage for storing key validation data.
   */
  const KEY_APIKEY = 'akismet_test_server_apikey';

  /**
   * Key to use for addressing state storage for storing feedback data.
   */
  const KEY_FEEDBACK = 'akismet_test_server_feedback';

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * ServerController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The UUID service.
   */
  public function __construct(Request $request, UuidInterface $uuid) {
    $this->request = $request;
    $this->uuid = $uuid;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('uuid')
    );
  }

  /**
   * Route callback for verify-key.
   */
  public function verifyKey(): Response {
    $fields = [
      'key' => FALSE,
      'blog' => TRUE,
    ];
    $data = $this->extractAndStoreRequestData($fields, self::KEY_APIKEY);

    $validKey = strcmp($data['key'], 'validkey') === 0;

    if ($validKey) {
      return $this->serializeResponseData('valid');
    }
    else {
      return $this->serializeResponseData('invalid');
    }
  }

  /**
   * Route callback for comment-check.
   */
  public function commentcheck(): Response {
    // Fieldss to pull from the request. Keys are fields, values are whether
    // the input is mandatory.
    $fields = [
      'blog' => TRUE,
      'comment_author' => FALSE,
      'comment_author_email' => FALSE,
      'comment_author_id' => FALSE,
      'comment_content' => FALSE,
      'user_ip' => TRUE,
    ];

    $data = $this->extractAndStoreRequestData($fields, self::KEY_CONTENT);

    $content = implode('', $data);

    // For a spam response without the 'pro tip", submit 'unsure'.
    if (strpos($content, 'unsure') !== FALSE) {
      return $this->serializeResponseData('true');
    }
    // For a definite spam response, pass 'spam'. This will also trigger the
    // 'pro tip' functionality.
    if (strpos($content, 'spam') !== FALSE) {
      $response = $this->serializeResponseData('true');
      $response->headers->set('X-akismet-pro-tip', 'discard');

      return $response;
    }

    // For a ham response, submit anything else. Or use 'ham' if you like.
    return $this->serializeResponseData('false');
  }

  /**
   * Route callback for submit-ham.
   */
  public function submitHam(): Response {
    $blog = $this->request->get('blog');
    $userIp = $this->request->get('user_ip');

    $this->extractAndStoreRequestData(
      [
        'comment_content' => FALSE,
        'blog' => TRUE,
        'user_ip' => TRUE,
      ],
      self::KEY_FEEDBACK,
      ['reason' => 'spam']
    );

    if (empty($blog)) {
      throw new \InvalidArgumentException('Blog argument must be set.');
    }

    if (empty($userIp)) {
      throw new \InvalidArgumentException('User IP argument must be set.');
    }

    return $this->serializeResponseData('Thanks for making the web a better place.');
  }

  /**
   * Route callback for submit-spam.
   */
  public function submitSpam(): Response {
    // Just do the same thing as when submitting ham; we don't actually do
    // anything and the response is the same.
    return $this->submitHam();
  }

  /**
   * Serializes the response data in xml format.
   *
   * @param string $data
   *   The data to serialize.
   * @param int $code
   *   The HTTP response code to return.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response for chaining.
   */
  protected function serializeResponseData(string $data, int $code = Response::HTTP_OK): Response {
    $response = new Response($data, $code);
    $response->headers->set('Content-Type', 'text/plain; charset=utf-8');
    $response->headers->set('X-Akismet-Guid', $this->uuid->generate());
    return $response;
  }

  /**
   * Extract variables from a request and store them in the state storage.
   *
   * @param array $fields
   *   Fields to extract from the request.
   * @param string $key
   *   State key to store the data in.
   * @param array $extra
   *   Extra data to store. Optional.
   *
   * @return array
   *   The extracted data.
   */
  private function extractAndStoreRequestData(array $fields, string $key, array $extra = []): array {
    $data = $extra;

    foreach ($fields as $field => $mandatory) {
      $value = $this->request->get($field);

      if (!empty($value)) {
        $data[$field] = $value;
      }
      elseif ($mandatory) {
        throw new \InvalidArgumentException("Input '$field' must be set.");
      }
    }

    // Store the request data so we can retrieve it from a test.
    $storage = $this->state()->get($key, []);
    $contentId = (!empty($data['id']) ? $data['id'] : md5(mt_rand()));
    if (isset($storage[$contentId])) {
      $storage[$contentId] = array_merge($storage[$contentId], $data);
    }
    else {
      $storage[$contentId] = $data;
    }
    $this->state()->set($key, $storage);
    return $data;
  }

}
